package net.thumbtack.onlineshop.controllersTest;


import net.thumbtack.onlineshop.TestUtil;
import net.thumbtack.onlineshop.controller.AdminController;
import net.thumbtack.onlineshop.controller.RegistrationController;
import net.thumbtack.onlineshop.model.Category;
import net.thumbtack.onlineshop.model.User;
import net.thumbtack.onlineshop.model.modelRequest.CategoryRequest;
import net.thumbtack.onlineshop.model.modelRequest.LoginUserRequest;
import net.thumbtack.onlineshop.model.modelRequest.UserRequest;
import net.thumbtack.onlineshop.model.modelResponse.AdminResponse;
import net.thumbtack.onlineshop.model.modelResponse.CategoryResponse;
import net.thumbtack.onlineshop.model.modelResponse.ClientResponse;
import net.thumbtack.onlineshop.model.modelResponse.UserResponse;
import net.thumbtack.onlineshop.security.Role;
import net.thumbtack.onlineshop.service.CategoryService;
import net.thumbtack.onlineshop.service.RegistrationService;
import net.thumbtack.onlineshop.validate.UserRequestValidation;
import org.hibernate.Session;
import org.hibernate.internal.SessionImpl;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static net.thumbtack.onlineshop.validate.UserRequestValidation.userRequestValidation;
import static org.hamcrest.Matchers.*;
import static org.junit.Assert.assertNotNull;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.when;
import static org.springframework.security.test.web.servlet.response.SecurityMockMvcResultMatchers.authenticated;
import static org.springframework.security.test.web.servlet.response.SecurityMockMvcResultMatchers.unauthenticated;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class AdminControllerTest {
    @MockBean
    private CategoryService categoryServiceMock;
    @Autowired
    private MockMvc mockMvc;
    @Autowired
    private AdminController adminController;

    private CategoryRequest categoryRequest1 = new CategoryRequest("category_1");
    private CategoryRequest categoryRequest2 = new CategoryRequest("subCat_1", 1);
    private CategoryRequest categoryRequest3 = new CategoryRequest("subCat_2", 1);

    @Test
    public void addCategoryTest() throws Exception {
        Category category1 = new Category(categoryRequest1);
        category1.setId(1);

        when(categoryServiceMock.addCategory(any())).thenReturn(new CategoryResponse(category1));

        this.mockMvc.perform(post("/api/categories")
                .cookie(new Cookie("JAVASESSIONID", String.valueOf(1)))
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(categoryRequest1)))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id", is(1)))
                .andExpect(jsonPath("$.categoryName", is(category1.getName())));
    }

    @Test
    public void getCategoryByNumberTest() throws Exception {
        Category category1 = new Category(categoryRequest1);
        category1.setId(1);
        CategoryResponse categoryResponse1 = new CategoryResponse(category1);
        Category subCat1 = new Category(categoryRequest2);
        subCat1.setId(2);
        subCat1.setParentCategoryId(1);
        CategoryResponse subCatResponse1 = new CategoryResponse(subCat1);
        Category subCat2 = new Category(categoryRequest3);
        subCat2.setId(3);
        subCat2.setParentCategoryId(1);
        CategoryResponse subCatResponse2 = new CategoryResponse(subCat2);

        when(categoryServiceMock.getListCategories()).thenReturn(
                new ArrayList<>(Arrays.asList(categoryResponse1, subCatResponse1, subCatResponse2)));
        when(categoryServiceMock.getCategoryByNumber(1)).thenReturn(new CategoryResponse(subCat1));

        this.mockMvc.perform(get("/api/categories/{num}", 1)
                .cookie(new Cookie("JAVASESSIONID", String.valueOf(1))))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id", is(2)))
                .andExpect(jsonPath("$.categoryName", is(subCat1.getName())));
    }

    @Test
    public void editCategoryByNumber() throws Exception {
        Category category1 = new Category(categoryRequest1);
        category1.setId(1);
        CategoryResponse categoryResponse1 = new CategoryResponse(category1);
        Category subCat1 = new Category(categoryRequest2);
        subCat1.setId(2);
        subCat1.setParentCategoryId(1);
        CategoryResponse subCatResponse1 = new CategoryResponse(subCat1);
        CategoryResponse subCatResponse2 = subCatResponse1;
        subCatResponse2.setCategoryName(categoryRequest3.getCategoryName());

        when(categoryServiceMock.getListCategories()).thenReturn(
                new ArrayList<>(Arrays.asList(categoryResponse1, subCatResponse1)));
        when(categoryServiceMock.editCategoryByNumber(anyInt(), any())).thenReturn(subCatResponse2);

        this.mockMvc.perform(get("/api/categories/{num}", 1)
                .cookie(new Cookie("JAVASESSIONID", String.valueOf(1)))
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(categoryRequest3)))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id", is(2)))
                .andExpect(jsonPath("$.categoryName", is(categoryRequest3.getCategoryName())));
    }

    @Test
    public void delCategoryById() throws Exception {
        Category category1 = new Category(categoryRequest1);
        category1.setId(1);
        CategoryResponse categoryResponse1 = new CategoryResponse(category1);
        Category subCat1 = new Category(categoryRequest2);
        subCat1.setId(2);
        subCat1.setParentCategoryId(1);
        CategoryResponse subCatResponse1 = new CategoryResponse(subCat1);
        Category subCat2 = new Category(categoryRequest3);
        subCat2.setId(3);
        subCat2.setParentCategoryId(1);
        CategoryResponse subCatResponse2 = new CategoryResponse(subCat2);

        when(categoryServiceMock.getListCategories()).thenReturn(
                new ArrayList<>(Arrays.asList(categoryResponse1, subCatResponse1, subCatResponse2)));

        this.mockMvc.perform(delete("/api/categories/{id}", 2)
                .cookie(new Cookie("JAVASESSIONID", String.valueOf(1))))
                .andDo(print())
                .andExpect(status().isOk());
    }

    @Test
    public void getListCategoriesTest() throws Exception {
        Category category1 = new Category(categoryRequest1);
        category1.setId(1);
        CategoryResponse categoryResponse1 = new CategoryResponse(category1);
        Category subCat1 = new Category(categoryRequest2);
        subCat1.setId(2);
        subCat1.setParentCategoryId(1);
        CategoryResponse subCatResponse1 = new CategoryResponse(subCat1);
        Category subCat2 = new Category(categoryRequest3);
        subCat2.setId(3);
        subCat2.setParentCategoryId(1);
        CategoryResponse subCatResponse2 = new CategoryResponse(subCat2);

        when(categoryServiceMock.getListCategories()).thenReturn(
                new ArrayList<>(Arrays.asList(categoryResponse1, subCatResponse1, subCatResponse2)));

        this.mockMvc.perform(get("/api/categories/")
                .cookie(new Cookie("JAVASESSIONID", String.valueOf(1))))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(3)))
                .andExpect(jsonPath("$[0].id", is(1)))
                .andExpect(jsonPath("$[0].categoryName", is(category1.getName())))
                .andExpect(jsonPath("$[1].id", is(2)))
                .andExpect(jsonPath("$[1].categoryName", is(subCat1.getName())))
                .andExpect(jsonPath("$[2].id", is(3)))
                .andExpect(jsonPath("$[2].categoryName", is(subCat2.getName())));
    }
}
