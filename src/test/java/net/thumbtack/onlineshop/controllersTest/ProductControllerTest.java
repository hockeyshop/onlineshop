package net.thumbtack.onlineshop.controllersTest;


import net.thumbtack.onlineshop.TestUtil;
import net.thumbtack.onlineshop.controller.AdminController;
import net.thumbtack.onlineshop.controller.ProductController;
import net.thumbtack.onlineshop.controller.RegistrationController;
import net.thumbtack.onlineshop.model.Category;
import net.thumbtack.onlineshop.model.Product;
import net.thumbtack.onlineshop.model.User;
import net.thumbtack.onlineshop.model.modelRequest.CategoryRequest;
import net.thumbtack.onlineshop.model.modelRequest.LoginUserRequest;
import net.thumbtack.onlineshop.model.modelRequest.ProductRequest;
import net.thumbtack.onlineshop.model.modelRequest.UserRequest;
import net.thumbtack.onlineshop.model.modelResponse.*;
import net.thumbtack.onlineshop.security.Role;
import net.thumbtack.onlineshop.service.CategoryService;
import net.thumbtack.onlineshop.service.ProductService;
import net.thumbtack.onlineshop.service.RegistrationService;
import net.thumbtack.onlineshop.validate.UserRequestValidation;
import org.hibernate.Session;
import org.hibernate.internal.SessionImpl;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static net.thumbtack.onlineshop.validate.UserRequestValidation.userRequestValidation;
import static org.hamcrest.Matchers.*;
import static org.junit.Assert.assertNotNull;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.when;
import static org.springframework.security.test.web.servlet.response.SecurityMockMvcResultMatchers.authenticated;
import static org.springframework.security.test.web.servlet.response.SecurityMockMvcResultMatchers.unauthenticated;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class ProductControllerTest {
    @MockBean
    private ProductService productServiceMock;
    @Autowired
    private MockMvc mockMvc;
    @Autowired
    private ProductController productController;

    List<Category> categoryList = new ArrayList<>(Arrays.asList(new Category("cat_1"), new Category("cat_2")));
    List<Integer> categoryIdList = new ArrayList<>(Arrays.asList(2, 3));
    private ProductRequest productRequest1 = new ProductRequest("Product_1", 10, 100);
    private ProductRequest productRequest2 = new ProductRequest("Product_2", 5, 10, categoryIdList);
    private ProductRequest productRequest3 = new ProductRequest("Product_3", 1, 1);

    @Test
    public void addProductTest() throws Exception {
        Product product1 = new Product(productRequest2);
        product1.setCategories(categoryList);
        product1.setId(1);

        when(productServiceMock.addProduct(any())).thenReturn(new ProductResponse(product1, categoryIdList, null));

        this.mockMvc.perform(post("/api/products")
                .cookie(new Cookie("JAVASESSIONID", String.valueOf(1)))
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(productRequest2)))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id", is(1)))
                .andExpect(jsonPath("$.name", is(product1.getName())))
                .andExpect(jsonPath("$.price", is(product1.getPrice())))
                .andExpect(jsonPath("$.count", is(product1.getCount())))
                .andExpect(jsonPath("$.categoriesId", hasSize(2)))
                .andExpect(jsonPath("$.categoriesId[0]", is(categoryIdList.get(0))))
                .andExpect(jsonPath("$.categoriesId[1]", is(categoryIdList.get(1))));
    }
}
