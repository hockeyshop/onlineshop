package net.thumbtack.onlineshop.controllersTest;

import net.thumbtack.onlineshop.TestUtil;
import net.thumbtack.onlineshop.controller.RegistrationController;
import net.thumbtack.onlineshop.model.User;
import net.thumbtack.onlineshop.model.modelRequest.LoginUserRequest;
import net.thumbtack.onlineshop.model.modelRequest.UserRequest;
import net.thumbtack.onlineshop.model.modelResponse.AdminResponse;
import net.thumbtack.onlineshop.model.modelResponse.ClientResponse;
import net.thumbtack.onlineshop.model.modelResponse.UserResponse;
import net.thumbtack.onlineshop.security.Role;
import net.thumbtack.onlineshop.service.RegistrationService;
import net.thumbtack.onlineshop.validate.UserRequestValidation;
import org.hibernate.Session;
import org.hibernate.internal.SessionImpl;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.security.core.Authentication;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static net.thumbtack.onlineshop.validate.UserRequestValidation.userRequestValidation;
import static org.hamcrest.Matchers.*;
import static org.junit.Assert.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;
import static org.springframework.security.test.web.servlet.response.SecurityMockMvcResultMatchers.authenticated;
import static org.springframework.security.test.web.servlet.response.SecurityMockMvcResultMatchers.unauthenticated;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class RegistrationControllerTest {
    @MockBean
    private RegistrationService registrationServiceMock;
    @Autowired
    private MockMvc mockMvc;
    @Autowired
    private RegistrationController registrationController;
    @MockBean
    private UserRequestValidation userRequestValidation;

    private UserRequest userRequest1 = new UserRequest("Иван", "Петров", "Сергеевич",
            "email_111@mail.ru", "Саратов", "+7-927 111 2233", "Логин111", "Pass111");
    private UserRequest userRequest2 = new UserRequest("Джон", "Фишман", null,
            "email_222@mail.ru", "Washington", "8 917 444 5566", "Логин222", "Pass222");
    private UserRequest adminRequest1 = new UserRequest("Сергей", "Сурков", "Сергеевич",
            "administrator", "Логин333", "Pass333");

    private Role userRole = new Role(1, "ROLE_USER");
    private Role adminRole = new Role(2, "ROLE_ADMIN");



    @Test
    public void addClientTest() throws Exception {
        User user = new User(userRequestValidation(userRequest1, null));
        user.setId(1);

        when(registrationServiceMock.saveUser(any())).thenReturn(new ClientResponse(user));
        when(registrationServiceMock.userLogin(anyString(), anyString())).thenReturn(new ClientResponse(user));
        when(registrationServiceMock.getUserById(any())).thenReturn(user);

        this.mockMvc.perform(post("/api/clients")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(userRequest1)))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.firstName").value("Иван"))
                .andExpect(jsonPath("$.lastName", is("Петров")))
                .andExpect(jsonPath("$.patronymic", is("Сергеевич")))
                .andExpect(jsonPath("$.email", is("email_111@mail.ru")))
                .andExpect(jsonPath("$.address", is("Саратов")))
                .andExpect(jsonPath("$.phone", is("89271112233")))
                .andExpect(jsonPath("$.deposit").value(nullValue()))
                .andExpect(jsonPath("$.deposit").doesNotExist())
                .andExpect(cookie().value("JAVASESSIONID", "1"));
    }

    @Test
    public void addAdminTest() throws Exception {
        User user = new User(userRequestValidation(adminRequest1, null));
        user.setId(1);

//        when(registrationServiceMock.saveUser(userRequest1)).thenReturn(user);
        when(registrationServiceMock.saveAdmin(any())).thenReturn(new AdminResponse(user));
        when(registrationServiceMock.userLogin(anyString(), anyString())).thenReturn(new AdminResponse(user));
        when(registrationServiceMock.getUserById(any())).thenReturn(user);

        this.mockMvc.perform(post("/api/admins")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(adminRequest1)))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.firstName").value(adminRequest1.getFirstName()))
                .andExpect(jsonPath("$.lastName", is(adminRequest1.getLastName())))
                .andExpect(jsonPath("$.patronymic", is(adminRequest1.getPatronymic())))
                .andExpect(jsonPath("$.position", is(adminRequest1.getPosition())))
                .andExpect(cookie().value("JAVASESSIONID", "1"));

    }

    @Test
    public void loginTest() throws Exception {
        User user = new User(userRequestValidation(adminRequest1, null));
        LoginUserRequest loginRequest = new LoginUserRequest("Логин333", "Pass333");
        user.setId(1);
        user.getRoles().add(adminRole);

        UserResponse userResponse = new AdminResponse(user);

        when(registrationServiceMock.saveAdmin(any())).thenReturn(userResponse);
        when(registrationServiceMock.userLogin(anyString(), anyString())).thenReturn(userResponse);
        when(registrationServiceMock.getUserById(any())).thenReturn(user);

        this.mockMvc.perform(post("/api/sessions")
                .contentType(MediaType.APPLICATION_JSON)
                .content("{\"login\":\"Логин333\", \"password\":\"Pass333\"}")
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(authenticated())
                .andExpect(jsonPath("$.firstName").value(adminRequest1.getFirstName()))
                .andExpect(jsonPath("$.lastName", is(adminRequest1.getLastName())))
                .andExpect(jsonPath("$.patronymic", is(adminRequest1.getPatronymic())))
                .andExpect(jsonPath("$.position", is(adminRequest1.getPosition())))
                .andExpect(cookie().value("JAVASESSIONID", "1"));
    }

    @Test
    public void userLogoutTest() throws Exception {
        User user = new User(userRequestValidation(adminRequest1, null));
        user.setId(1);
        user.getRoles().add(adminRole);

//        registrationServiceMock.saveAdmin(any());

        when(registrationServiceMock.saveAdmin(any())).thenReturn(new UserResponse(user));
        when(registrationServiceMock.userLogin(anyString(), anyString())).thenReturn(new UserResponse(user));
        when(registrationServiceMock.getUserById(any())).thenReturn(user);

        this.mockMvc.perform(get("/api/sessions"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(unauthenticated());
    }

    @Test
    public void findUserTest() throws Exception {
        User user = new User(userRequestValidation(userRequest1, null));
        user.setId(1);
        user.getRoles().add(userRole);

       ClientResponse userResponse = new ClientResponse(user);

        when(registrationServiceMock.findUserById(any())).thenReturn(userResponse);

//        MockHttpServletResponse mockResponse = new MockHttpServletResponse();
//        mockResponse.addCookie(new Cookie("JAVASESSIONID", String.valueOf(1)));
//        registrationController.userLogin(new LoginUserRequest(user.getLogin(), user.getPassword()), mockResponse);

        this.mockMvc.perform(get("/api/accounts").cookie(new Cookie("JAVASESSIONID", String.valueOf(1))))
//                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.firstName").value(userResponse.getFirstName()))
                .andExpect(jsonPath("$.lastName", is(userResponse.getLastName())))
                .andExpect(jsonPath("$.patronymic", is(userResponse.getPatronymic())))
                .andExpect(jsonPath("$.email", is(userResponse.getEmail())))
                .andExpect(jsonPath("$.address", is(userResponse.getAddress())))
                .andExpect(jsonPath("$.phone", is(userResponse.getPhone())));
    }

    @Test
    public void findAllUserTest() throws Exception {
        User user1 = new User(userRequestValidation(userRequest1, null));
        user1.setId(1);
        user1.getRoles().add(userRole);
        User user2 = new User(userRequestValidation(userRequest2, null));
        user2.setId(2);
        user2.getRoles().add(userRole);
        User admin1 = new User(userRequestValidation(adminRequest1, null));
        admin1.setId(1);
        admin1.getRoles().add(userRole);

        List<UserResponse> userResponseList = new ArrayList<>();
        userResponseList.add(new ClientResponse(user1));
        userResponseList.add(new ClientResponse(user2));
        userResponseList.add(new AdminResponse(admin1));

        when(registrationServiceMock.findAllUsers()).thenReturn(userResponseList);

        this.mockMvc.perform(get("/api/clients"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(TestUtil.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$", hasSize(3)))
                .andExpect(jsonPath("$[0].firstName").value(userResponseList.get(0).getFirstName()))
                .andExpect(jsonPath("$[1].firstName").value(userResponseList.get(1).getFirstName()))
                .andExpect(jsonPath("$[2].firstName").value(userResponseList.get(2).getFirstName()));

    }

    @Test
    public void editAdminTest() throws Exception {
        User admin1 = new User(userRequestValidation(adminRequest1, null));
        admin1.setId(1);
        admin1.getRoles().add(userRole);

        admin1.setFirstName("Николай");
        admin1.setLastName("Федоров");

        when(registrationServiceMock.updateAdmin(any(), any())).thenReturn(new AdminResponse(admin1));

        this.mockMvc.perform(put("/api/admins")
                .cookie(new Cookie("JAVASESSIONID", String.valueOf(1)))
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(adminRequest1)))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.firstName").value("Николай"))
                .andExpect(jsonPath("$.lastName", is("Федоров")))
                .andExpect(jsonPath("$.patronymic", is(adminRequest1.getPatronymic())))
                .andExpect(jsonPath("$.position", is(adminRequest1.getPosition())));
    }

    @Test
    public void editUserTest() throws Exception {
        User user1 = new User(userRequestValidation(userRequest1, null));
        user1.setId(1);
        user1.getRoles().add(userRole);

        user1.setFirstName("Николай");
        user1.setLastName("Федоров");

        when(registrationServiceMock.updateUser(any(), any())).thenReturn(new ClientResponse(user1));

        this.mockMvc.perform(put("/api/clients").cookie(new Cookie("JAVASESSIONID", String.valueOf(1)))
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(userRequest1)))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.firstName").value("Николай"))
                .andExpect(jsonPath("$.lastName", is("Федоров")))
                .andExpect(jsonPath("$.patronymic", is(user1.getPatronymic())))
                .andExpect(jsonPath("$.email", is(user1.getEmail())))
                .andExpect(jsonPath("$.address", is(user1.getAddress())))
                .andExpect(jsonPath("$.phone", is(user1.getPhone())));
    }






//    @Test
    public void test() throws Exception {
        assertNotNull(registrationController);
        this.mockMvc.perform(get("/"))
                .andDo(print())
                .andExpect(status().isOk());
    }

    //    @Test
    public void accessDeniedTest() throws Exception {
        this.mockMvc.perform(get("/find/**"))
                .andDo(print())
                .andExpect(status().is4xxClientError());
    }

//    @Test // Второй вариант теста на добавление нового пользователя
    public void addUserTestVar2() throws Exception {
       /* UserRequest request = new UserRequest("firstName_1", "lastName_1", "patronymic_1",
                "email_1@mail.ru", "address_1", "89218887711", "login_1", "11111");

        request.setFirstName("firstName_1");
        request.setLastName("lastName_1");
        request.setLogin("login_1");
        request.setPassword("11111");

        User user = new User(1, "firstName_1", "lastName_1", null,
                "email_1@mail.ru", "address_1", "89218887711", "login_1", "11111");

        user.setId(1);
        user.setFirstName("firstName_1");
        user.setLastName("lastName_1");
        user.setLogin("login_1");
        user.setPassword("11111");

        User response = registrationController.userAdd(request);

        when(userServiceMock.saveUser(request)).thenReturn(user);

        this.mockMvc.perform(post("/api/clients")
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content("{'firstName':'firstName_1', 'lastName':'lastName_1', 'patronymic':'patronymic_1'" +
                        "  'email':'email_1@mail.ru', 'address':'address_1', 'phone':'89218887711'," +
                        "  'login':'login_1', 'password':'11111'}")
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isOk());
//                .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
//                .andExpect((ResultMatcher) jsonPath("$.id").value(1))
//                .andExpect((ResultMatcher) jsonPath("$.firstName").value("firstName_1"))
//                .andExpect((ResultMatcher) jsonPath("$.lastName").value("lastName_1"));*/
    }

}