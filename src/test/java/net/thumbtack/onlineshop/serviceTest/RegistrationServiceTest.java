package net.thumbtack.onlineshop.serviceTest;


import net.thumbtack.onlineshop.controller.RegistrationController;
import net.thumbtack.onlineshop.exeptions.UserException;
import net.thumbtack.onlineshop.model.User;
import net.thumbtack.onlineshop.model.modelRequest.UserRequest;
import net.thumbtack.onlineshop.model.modelResponse.ClientResponse;
import net.thumbtack.onlineshop.model.modelResponse.UserResponse;
import net.thumbtack.onlineshop.repository.RoleRepository;
import net.thumbtack.onlineshop.repository.UserRepository;
import net.thumbtack.onlineshop.security.Role;
import net.thumbtack.onlineshop.service.RegistrationService;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

@RunWith(SpringRunner.class)
@SpringBootTest
public class RegistrationServiceTest {
    @Autowired
    RegistrationService registrationService;
    @Autowired
    private RegistrationController registrationController;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private RoleRepository roleRepository;

    private UserRequest userRequest1 = new UserRequest("Иван", "Петров", "Сергеевич",
            "email_111@mail.ru", "Саратов", "+7-927-111-22-33", "Логин111", "Pass111");
    private UserRequest userRequest2 = new UserRequest("Джон", "Фишман", null,
            "email_222@mail.ru", "Washington", "8 917 444 5566", "Логин222", "Pass222");
    private UserRequest adminRequest1 = new UserRequest("Сергей", "Сурков", "Сергеевич",
            "administrator", "Логин333", "Pass333");
    private Role roleAdmin = new Role(2, "ROLE_ADMIN");

//    @Before
//    @Transactional
//    public void setUp(){
//        userRepository.deleteAll();
//    }

    @Test
    @Transactional
    public void saveAdminTest() {
        userRepository.deleteAll();

        UserResponse adminResponse = registrationService.saveAdmin(adminRequest1);
//        UserResponse adminResponse1 = new UserResponse(admin);

        assertEquals(1, userRepository.findAll().size());
        assertTrue(userRepository.getOne(adminResponse.getId()).getRoles().contains(roleRepository.getOne(2)));
    }

    @Test
    @Transactional
    public void saveUserTest() {
        userRepository.deleteAll();

        ClientResponse clientResponse1 = (ClientResponse) registrationService.saveUser(userRequest1);
        ClientResponse clientResponse2 = (ClientResponse) registrationService.saveUser(userRequest2);

        assertEquals(2, userRepository.findAll().size());
        assertTrue(userRepository.getOne(clientResponse1.getId()).getRoles().contains(roleRepository.getOne(1)));
        assertTrue(userRepository.getOne(clientResponse2.getId()).getRoles().contains(roleRepository.getOne(1)));
//        assertTrue(clientResponse2.getPhone().matches("^[8](\\d){10}$"));

    }

    @Test
    @Transactional
    public void updateUserTest() {
        userRepository.deleteAll();
        ClientResponse clientResponse1 = (ClientResponse) registrationService.saveUser(userRequest1);
        User userFromDB = userRepository.findAll().get(0);

        assertEquals(1, userRepository.findAll().size());
        assertEquals(userRequest1.getLogin(), userFromDB.getLogin());
        assertEquals(userRequest1.getPassword(), userFromDB.getPassword());

        userRequest1.setFirstName("Роман");
        userRequest1.setLastName("Поляков");
        UserResponse updateUserResponse = registrationService.updateUser(userRequest1, clientResponse1.getId());
        User updateUser = userRepository.getOne(updateUserResponse.getId());
        userFromDB = userRepository.findAll().get(0);

        assertEquals(1, userRepository.findAll().size());
        assertEquals("Роман", userFromDB.getFirstName());
        assertEquals("Поляков", userFromDB.getLastName());
        assertTrue(updateUser.getRoles().contains(roleRepository.getOne(1)));


    }

    @Test
    @Transactional
    public void updateAdminTest() {
        userRepository.deleteAll();
        UserResponse adminResponse = registrationService.saveAdmin(adminRequest1);
        User adminFromDB = userRepository.findAll().get(0);

        assertEquals(1, userRepository.findAll().size());
        assertEquals(adminRequest1.getLogin(), adminFromDB.getLogin());
        assertEquals(adminRequest1.getPassword(), adminFromDB.getPassword());

        adminRequest1.setFirstName("Роман");
        adminRequest1.setLastName("Поляков");
        Integer id = adminResponse.getId();

        UserResponse adminUpdatedResponse = registrationService.updateUser(adminRequest1, id);
        User adminUpdated = userRepository.getOne(adminUpdatedResponse.getId());
        adminFromDB = userRepository.findAll().get(0);

        assertEquals(1, userRepository.findAll().size());
        assertEquals("Роман", adminFromDB.getFirstName());
        assertTrue(adminUpdated.getRoles().contains(roleRepository.getOne(2)));

    }

    @Test
    @Transactional
    public void userLogin() {
        userRepository.deleteAll();
        ClientResponse clientResponse1 = (ClientResponse) registrationService.saveUser(userRequest1);
        User userFromDB = userRepository.findAll().get(0);

        assertEquals(userRepository.getOne(clientResponse1.getId()).getLogin(), userRepository.findByLogin(userRepository.getOne(clientResponse1.getId()).getLogin()).getLogin());
    }

    @Test
    @Transactional
    public void userLoginWrong() throws RuntimeException {
        userRepository.deleteAll();
        ClientResponse clientResponse1 = (ClientResponse) registrationService.saveUser(userRequest1);
        User userFromDB = userRepository.findAll().get(0);

        try {
            registrationService.userLogin(userRequest2.getLogin(), userRequest1.getPassword());
            Assert.fail("Expected UserException");
        } catch (UserException ex) {
            assertEquals("login", ex.getField());
        }
        try {
            registrationService.userLogin(userRequest1.getLogin(), userRequest2.getPassword());
            Assert.fail("Expected UserException");
        } catch (UserException ex) {
            assertEquals("password", ex.getField());
        }
    }

    @Test
    public void findAllUsers() {
        userRepository.deleteAll();

        assertEquals(0, userRepository.findAll().size());

        ClientResponse clientResponse1 = (ClientResponse) registrationService.saveUser(userRequest1);
        ClientResponse clientResponse2 = (ClientResponse) registrationService.saveUser(userRequest2);

        assertEquals(2, userRepository.findAll().size());
        assertEquals(userRequest1.getLogin(), userRepository.findAll().get(0).getLogin());
        assertEquals(userRequest1.getLogin(), userRepository.findAll().get(0).getLogin());
    }

    @Test
    public void findUserById() {
        userRepository.deleteAll();

        UserResponse adminResponse = registrationService.saveAdmin(adminRequest1);
        assertEquals(1, userRepository.findAll().size());
        assertEquals(adminRequest1.getLogin(), registrationService.getUserById(adminResponse.getId()).getLogin());
        assertEquals(adminRequest1.getPassword(), registrationService.getUserById(adminResponse.getId()).getPassword());
    }

    @Test
    public void userDeleteById() {
        userRepository.deleteAll();

        UserResponse adminResponse = registrationService.saveAdmin(adminRequest1);
        assertEquals(1, userRepository.findAll().size());

        registrationService.userDeleteById(adminResponse.getId());
        assertEquals(0, userRepository.findAll().size());
    }

}