package net.thumbtack.onlineshop.serviceTest;

import net.thumbtack.onlineshop.model.modelRequest.CategoryRequest;
import net.thumbtack.onlineshop.model.modelResponse.CategoryResponse;
import net.thumbtack.onlineshop.repository.CategoryRepository;
import net.thumbtack.onlineshop.service.CategoryService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

@RunWith(SpringRunner.class)
@SpringBootTest
public class CategoryServiceTest {
    @Autowired
    private CategoryService categoryService;
    @Autowired
    private CategoryRepository categoryRepository;


    @Test
    public void addCategoryTest() {
        categoryRepository.deleteAll();
        CategoryResponse categoryResponse = categoryService.addCategory("category_1");

        assertEquals(1, categoryRepository.findAll().size());
        assertEquals("category_1", categoryResponse.getCategoryName());
    }

    @Test
    @Transactional
    public void addSubCategoryTest() {
        categoryRepository.deleteAll();
        CategoryResponse categoryResponse = categoryService.addCategory("category_1");
        CategoryResponse subCatResponse1 = categoryService.addSubCategory("subCat_1", categoryResponse.getId());
        CategoryResponse subCatResponse2 = categoryService.addSubCategory("subCat_2", categoryResponse.getId());

        assertEquals(3, categoryRepository.findAll().size());
        assertEquals("category_1", categoryResponse.getCategoryName());
        assertEquals("subCat_1", categoryRepository.getOne(subCatResponse1.getId()).getName());
        assertEquals("subCat_2", categoryRepository.getOne(subCatResponse2.getId()).getName());
        assertEquals(categoryResponse.getId(), categoryRepository.getOne(subCatResponse1.getId()).getParentCategoryId());
        assertEquals(categoryResponse.getId(), categoryRepository.getOne(subCatResponse2.getId()).getParentCategoryId());
    }

    @Test
    @Transactional
    public void getCategoryByNumberTest() {
        categoryRepository.deleteAll();
        CategoryResponse categoryResponse = categoryService.addCategory("parent_category");
        categoryService.addSubCategory("category_A", categoryResponse.getId());
        categoryService.addSubCategory("category_B", categoryResponse.getId());
        List<CategoryResponse> categoryList = categoryService.getListCategories();

        assertEquals(3, categoryRepository.findAll().size());
        assertEquals("parent_category", categoryService.getCategoryByNumber(0).getCategoryName());
        assertEquals("category_A", categoryService.getCategoryByNumber(1).getCategoryName());
        assertEquals("category_B", categoryService.getCategoryByNumber(2).getCategoryName());

    }

    @Test
    @Transactional
    public void getCategoryByIdTest() {
        categoryRepository.deleteAll();
        CategoryResponse categoryResponse = categoryService.addCategory("category_1");
        CategoryResponse subCatResponse1 = categoryService.addSubCategory("subCat_1", categoryResponse.getId());
        CategoryResponse subCatResponse2 = categoryService.addSubCategory("subCat_2", categoryResponse.getId());

        assertEquals(3, categoryRepository.findAll().size());
        assertEquals(categoryResponse.getCategoryName(), categoryRepository.getOne(categoryResponse.getId()).getName());
        assertEquals(subCatResponse1.getCategoryName(), categoryRepository.getOne(subCatResponse1.getId()).getName());
        assertEquals(subCatResponse2.getCategoryName(), categoryRepository.getOne(subCatResponse2.getId()).getName());
    }

    @Test
    @Transactional
    public void editCategoryByNumberTest() {
        categoryRepository.deleteAll();
        CategoryResponse categoryResponse1 = categoryService.addCategory("category_A");
        CategoryResponse categoryResponse2 = categoryService.addCategory("category_B");
        CategoryResponse subCatResponse1 = categoryService.addSubCategory("category_C", categoryResponse1.getId());
        CategoryResponse subCatResponse2 = categoryService.addSubCategory("category_D", categoryResponse1.getId());

        assertEquals("category_D", categoryRepository.getOne(subCatResponse2.getId()).getName());
        categoryService.editCategoryByNumber(2, new CategoryRequest("category_E"));

        assertEquals(4, categoryRepository.findAll().size());
        assertEquals("category_A", categoryRepository.getOne(categoryResponse1.getId()).getName());
        assertEquals("category_C", categoryRepository.getOne(subCatResponse1.getId()).getName());
        assertEquals("category_E", categoryRepository.getOne(subCatResponse2.getId()).getName());

        assertEquals(categoryResponse1.getId(), categoryRepository.getOne(subCatResponse2.getId()).getParentCategoryId());
        categoryService.editCategoryByNumber(2, new CategoryRequest(categoryResponse2.getId()));
        assertEquals("category_E", categoryRepository.getOne(subCatResponse2.getId()).getName());
        assertEquals(categoryResponse2.getId(), categoryRepository.getOne(subCatResponse2.getId()).getParentCategoryId());
    }

    @Test
    @Transactional
    public void delCategoryByIdTest() {
        categoryRepository.deleteAll();
        CategoryResponse categoryResponse = categoryService.addCategory("category_1");
        CategoryResponse subCatResponse1 = categoryService.addSubCategory("subCat_1", categoryResponse.getId());
        CategoryResponse subCatResponse2 = categoryService.addSubCategory("subCat_2", categoryResponse.getId());

        assertEquals(3, categoryRepository.findAll().size());
        categoryService.delCategoryById(subCatResponse1.getId());
        assertEquals(2, categoryRepository.findAll().size());
    }

    @Test
    @Transactional
    public void getListCategoriesTest() {
        categoryRepository.deleteAll();
        CategoryResponse categoryResponse1 = categoryService.addCategory("category_A");
        CategoryResponse categoryResponse2 = categoryService.addCategory("category_B");
        CategoryResponse subCatResponse1 = categoryService.addSubCategory("category_C", categoryResponse1.getId());
        CategoryResponse subCatResponse2 = categoryService.addSubCategory("category_D", categoryResponse1.getId());

        List<CategoryResponse> categoryResponseList = categoryService.getListCategories();
        assertEquals(4, categoryResponseList.size());
        assertEquals(categoryResponseList.get(0).getCategoryName(), categoryResponse1.getCategoryName());
        assertEquals(categoryResponseList.get(1).getCategoryName(), subCatResponse1.getCategoryName());
        assertEquals(categoryResponseList.get(2).getCategoryName(), subCatResponse2.getCategoryName());
        assertEquals(categoryResponseList.get(3).getCategoryName(), categoryResponse2.getCategoryName());

    }


}