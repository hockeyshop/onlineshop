package net.thumbtack.onlineshop.serviceTest;

import net.thumbtack.onlineshop.model.Category;
import net.thumbtack.onlineshop.model.Product;
import net.thumbtack.onlineshop.model.modelRequest.ProductRequest;
import net.thumbtack.onlineshop.model.modelResponse.ProductResponse;
import net.thumbtack.onlineshop.repository.CategoryRepository;
import net.thumbtack.onlineshop.repository.ProductRepository;
import net.thumbtack.onlineshop.service.CategoryService;
import net.thumbtack.onlineshop.service.ProductService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

import static java.util.Arrays.asList;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ProductServiceTest {
    List<Integer> categoryIdlist = new ArrayList<>();
    List<Category> categorylist = new ArrayList<>();
    @Autowired
    private CategoryService categoryService;
    @Autowired
    private CategoryRepository categoryRepository;
    @Autowired
    private ProductService productService;
    @Autowired
    private ProductRepository productRepository;

    @Test
    @Transactional
    public void addProductTest() {
        productRepository.deleteAll();
        productRepository.deleteAll();
        Category category1 = categoryRepository.saveAndFlush(new Category("category_A"));

        categoryIdlist.add(category1.getId());

        ProductResponse productResponse1 = productService.addProduct(new ProductRequest("Product_A", 5, 10));
        ProductResponse productResponse2 = productService.addProduct(new ProductRequest("Product_B", 5, 10, categoryIdlist));


        assertEquals(2, productRepository.findAll().size());
        assertEquals(0, productResponse1.getCategoriesId().size());
        assertEquals(1, productResponse2.getCategoriesId().size());
        assertEquals("category_A", categoryRepository.getOne(productResponse2.getCategoriesId().get(0)).getName());
    }

    @Test
    @Transactional
    public void updateProductTest() {
        productRepository.deleteAll();
        productRepository.deleteAll();
        Category category1 = categoryRepository.saveAndFlush(new Category("category_A"));
        Category category2 = categoryRepository.saveAndFlush(new Category("category_C", category1.getId()));

        categoryIdlist.add(category1.getId());

        ProductResponse productResponse1 = productService.addProduct(new ProductRequest("Product_A", 5, 10));

        assertEquals(1, productRepository.findAll().size());
        assertEquals(0, productRepository.getOne(productResponse1.getId()).getCategories().size());
        assertEquals("Product_A", productRepository.getOne(productResponse1.getId()).getName());

        ProductResponse productResponse2 = productService.updateProduct(productResponse1.getId(), new ProductRequest("Product_B", 5, 10, categoryIdlist));

        assertEquals(1, productRepository.findAll().size());
        assertEquals(1, productRepository.findAll().size());
        assertEquals(1, productResponse2.getCategoriesId().size());
        assertEquals(1, productRepository.getOne(productResponse1.getId()).getCategories().size());
        assertEquals("Product_B", productRepository.getOne(productResponse1.getId()).getName());
        assertEquals(2, categoryRepository.findAll().size());
    }

    @Test
    @Transactional
    public void deleteProductTest() {
        productRepository.deleteAll();
        categoryRepository.deleteAll();
        Category category1 = categoryRepository.saveAndFlush(new Category("category_A"));
        Category category2 = categoryRepository.saveAndFlush(new Category("category_C", category1.getId()));

        categoryIdlist.add(category1.getId());
        categoryIdlist.add(category2.getId());

        ProductResponse productResponse1 = productService.addProduct(new ProductRequest("Product_A", 5, 10));
        ProductResponse productResponse2 = productService.addProduct(new ProductRequest("Product_B", 5, 10, categoryIdlist));

        assertEquals(2, productRepository.findAll().size());
        assertEquals(2, categoryRepository.findAll().size());
        productService.deleteProduct(productResponse2.getId());
        assertEquals(1, productRepository.findAll().size());
        assertEquals(2, categoryRepository.findAll().size());
    }

    @Test
    @Transactional
    public void infoProductTest() {
        productRepository.deleteAll();
        productRepository.deleteAll();
        Category category1 = categoryRepository.saveAndFlush(new Category("category_A"));
        Category category2 = categoryRepository.saveAndFlush(new Category("category_C", category1.getId()));

        categorylist.add(category1);
        categorylist.add(category2);

        Product product = new Product("Product_B", 5, 10, categorylist);
        productRepository.saveAndFlush(product);
        ProductResponse productResponse = productService.infoProduct(product.getId());

        assertEquals(product.getId(), productResponse.getId());
        assertEquals(product.getName(), productResponse.getName());
        assertEquals(product.getPrice(), productResponse.getPrice());
        assertEquals(product.getCount(), productResponse.getCount());
        List<String> categoriesNameList = new ArrayList<>();
        categoriesNameList.add(category1.getName());
        categoriesNameList.add(category2.getName());
        assertTrue(productResponse.getCategoriesNames().containsAll(categoriesNameList));
    }

    @Test
    @Transactional
    public void infoProductListTest() {
        productRepository.deleteAll();
        categoryRepository.deleteAll();
        List<ProductResponse> productList;
        List<Integer> categories = null;
        String order = "";

        Category category1 = categoryRepository.saveAndFlush(new Category("category_A"));
        Category category2 = categoryRepository.saveAndFlush(new Category("category_C", category1.getId()));

        categorylist.add(category1);
        categorylist.add(category2);

        Product product1 = productRepository.saveAndFlush(new Product("Product_A", 5, 10, null));
        Product product2 = productRepository.saveAndFlush(new Product("Product_B", 5, 10, new ArrayList<>(asList(category1, category2))));
        Product product3 = productRepository.saveAndFlush(new Product("Product_C", 5, 10, new ArrayList<>(asList(category1))));
        Product product4 = productRepository.saveAndFlush(new Product("Product_D", 5, 10, new ArrayList<>(asList(category2))));

        productList = productService.infoProductList(null, "product");
        assertEquals(4, productList.size());
        assertEquals(product1.getName(), productList.get(0).getName());
        assertTrue(productList.get(0).getCategoriesNames().isEmpty());
        assertEquals(product2.getName(), productList.get(1).getName());
        assertEquals(2, productList.get(1).getCategoriesNames().size());
        assertEquals(product3.getName(), productList.get(2).getName());
        assertEquals(1, productList.get(2).getCategoriesNames().size());
        assertEquals(product4.getName(), productList.get(3).getName());

        productList = productService.infoProductList(new ArrayList<>(), "product");
        assertEquals(1, productList.size());
        assertEquals(product1.getName(), productList.get(0).getName());
        assertEquals(0, productList.get(0).getCategoriesNames().size());

        productList = productService.infoProductList(null, "category");
        assertEquals(5, productList.size());
        assertEquals(product1.getName(), productList.get(0).getName());
        assertEquals(0, productList.get(0).getCategoriesNames().size());

        assertEquals(product2.getName(), productList.get(1).getName());
        assertEquals(1, productList.get(1).getCategoriesNames().size());
        assertEquals("category_A", productList.get(1).getCategoriesNames().get(0));

        assertEquals(product3.getName(), productList.get(2).getName());
        assertEquals(1, productList.get(2).getCategoriesNames().size());
        assertEquals("category_A", productList.get(2).getCategoriesNames().get(0));

        assertEquals(product2.getName(), productList.get(3).getName());
        assertEquals(1, productList.get(3).getCategoriesNames().size());
        assertEquals("category_C", productList.get(3).getCategoriesNames().get(0));

        assertEquals(product4.getName(), productList.get(4).getName());
        assertEquals(1, productList.get(4).getCategoriesNames().size());
        assertEquals("category_C", productList.get(4).getCategoriesNames().get(0));

        productList = productService.infoProductList(new ArrayList<>(asList(category1.getId())), "product");
        assertEquals(2, productList.size());
        assertEquals(product2.getName(), productList.get(0).getName());
        assertTrue(productList.get(0).getCategoriesNames().contains("category_A"));
        assertEquals("category_A", productList.get(1).getCategoriesNames().get(0));

    }
}