package net.thumbtack.onlineshop.security;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jws;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import lombok.NonNull;
import org.springframework.stereotype.Component;

import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;
import java.util.Base64;
import java.util.Optional;

@Component
public class TokenHandler {
//    private final SecretKey secretKey;
    private String stringKey;

    public TokenHandler() {
        String jwtKey = "key123";
        String decodedKey = Base64.getEncoder().encodeToString(jwtKey.getBytes());
        stringKey = Base64.getEncoder().encodeToString(jwtKey.getBytes());
//        secretKey = new SecretKeySpec(decodedKey, 0, decodedKey.length(), "AES");
    }

    public Integer extractUserId(@NonNull String token) {
            Jws<Claims> claimsJws = Jwts.parser().setSigningKey(stringKey).parseClaimsJws(token);
            Claims body = claimsJws.getBody();
            Integer userId = Integer.valueOf(body.getId());
            return userId;
    }

    public String generateAccessToken(@NonNull Integer id) {
        return Jwts.builder()
                .setId(id.toString())
                .signWith(SignatureAlgorithm.HS512, stringKey)
                .compact();
    }
}
