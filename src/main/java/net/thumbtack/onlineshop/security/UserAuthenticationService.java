package net.thumbtack.onlineshop.security;

import lombok.NonNull;
import net.thumbtack.onlineshop.model.User;
import net.thumbtack.onlineshop.repository.UserRepository;
import net.thumbtack.onlineshop.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Service;
import org.springframework.web.util.WebUtils;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import java.util.Arrays;
import java.util.Optional;

import static org.springframework.web.util.WebUtils.getCookie;

@Service
public class UserAuthenticationService {
    private static final String COOKIE_NAME = "JAVASESSIONID";

    @Autowired
    private TokenHandler tokenHandler;
    @Autowired
    private UserDetailsServiceImpl userDetailsService;
    @Autowired
    private UserRepository userRepository;
    User user = new User();

    public Authentication getAuthentication(@NonNull HttpServletRequest request) {
        Cookie cookie = getCookie(request, COOKIE_NAME);
        if (cookie != null){
            Integer userId = tokenHandler.extractUserId(cookie.getValue());
//            User user = userRepository.getOne(userId);
            user = userDetailsService.findById(userId);
            return new UserAuthentication(user);
        }else {
            return null;
        }
    }
}
