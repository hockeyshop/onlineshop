package net.thumbtack.onlineshop.security;

import net.thumbtack.onlineshop.model.User;
import net.thumbtack.onlineshop.repository.RoleRepository;
import net.thumbtack.onlineshop.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class UserDetailsServiceImpl implements UserDetailsService {
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private RoleRepository roleRepository;


    @Override
    public UserDetails loadUserByUsername(String login) throws UsernameNotFoundException {
        User user = userRepository.findByLogin(login);
        if (user == null) {
            throw new UsernameNotFoundException("User not found");
        }
        return user;
    }

    @Transactional
    public User findById(Integer userId) throws UsernameNotFoundException {
        User user = userRepository.getOne(userId);
        if (user == null) {
            throw new UsernameNotFoundException("User not found");
        }
        return user;
    }
}
