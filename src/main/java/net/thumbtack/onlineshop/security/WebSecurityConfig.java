package net.thumbtack.onlineshop.security;

import net.thumbtack.onlineshop.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

@Configuration
@EnableWebSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {
    @Autowired
    private UserAuthenticationService userAuthenticationService;
    @Autowired
    private UserDetailsServiceImpl userDetailsServiceImpl;

    @Bean
    public BCryptPasswordEncoder bCryptPasswordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Override
    protected void configure(HttpSecurity httpSecurity) throws Exception {
        httpSecurity
                .csrf().disable()
                .addFilterAfter(new UserAuthenticationFilter(userAuthenticationService), UsernamePasswordAuthenticationFilter.class)
                .authorizeRequests()
//                .antMatchers("/").not().fullyAuthenticated()
                .antMatchers("/api/deposits", "/api/deposits/**",
                        "/api/baskets", "/api/baskets/**",
                        "/api/purchases", "/api/purchases/baskets")
                .hasRole("USER")
                .antMatchers("/api/categories", "/api/categories/**",
                        "/api/products", "/api/products/**")
                .hasRole("ADMIN")
                .antMatchers("/", "/api/sessions",
                        "/api/clients", "/api/admins", "/api/accounts")
                .permitAll()
                .anyRequest().authenticated();
    }

    @Autowired
    protected void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(userDetailsServiceImpl);
//        System.out.println(userDetails.);
    }

}
