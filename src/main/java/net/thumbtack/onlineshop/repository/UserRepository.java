package net.thumbtack.onlineshop.repository;

import net.thumbtack.onlineshop.model.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface UserRepository extends JpaRepository<User, Integer> {

//    User findByLoginAndPassword(String userName, String password);

    User findByLogin(String login);

    Optional<User> findById(Integer userId);

//    User findByFirstNameAndLastNameAndPosition(String firstName, String lastName, String position);

//    User findByFirstNameAndLastNameAndAddress(String firstName, String lastName, String address);

}
