package net.thumbtack.onlineshop.repository;

import net.thumbtack.onlineshop.security.Role;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RoleRepository extends JpaRepository<Role, Integer> {

}
