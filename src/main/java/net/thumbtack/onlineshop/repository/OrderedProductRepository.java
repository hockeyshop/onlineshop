package net.thumbtack.onlineshop.repository;

import net.thumbtack.onlineshop.model.OrderedProduct;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;

public interface OrderedProductRepository extends JpaRepository<OrderedProduct, Integer> {

}
