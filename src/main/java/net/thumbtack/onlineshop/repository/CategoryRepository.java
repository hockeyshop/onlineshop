package net.thumbtack.onlineshop.repository;

import net.thumbtack.onlineshop.model.Category;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface CategoryRepository extends JpaRepository<Category, Integer> {

    @Query(value = "SELECT * FROM categories WHERE parent_category_id IS NULL ORDER BY name", nativeQuery = true)
    List<Category> findParentCategories();

    @Query(value = "SELECT * FROM categories WHERE parent_category_id = :parentId ORDER BY name", nativeQuery = true)
    List<Category> findSubCategories(Integer parentId);

    List<Category> findAllByOrderByName();


//    @Query(value = "SELECT name FROM Categories", nativeQuery = true)
//    List<String> findNameFromCategories();
//
//    @Query(value = "SELECT id, name FROM categories WHERE id IN " +
//            "(SELECT category_id FROM categories_sub_categories WHERE sub_categories_id = :id);",
//            nativeQuery = true)
//    Category findParentCategory(@Param("id") Integer id);

}
