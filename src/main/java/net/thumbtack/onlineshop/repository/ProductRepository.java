package net.thumbtack.onlineshop.repository;

import net.thumbtack.onlineshop.model.Product;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface ProductRepository extends JpaRepository<Product, Integer> {

    @Query(value = "SELECT category_list_id FROM product_category_list WHERE product_id = :id",
            nativeQuery = true)
    List<Integer> getCategoryListByProductId(@Param("id") Integer id);

//    @Query(value = "SELECT product.*, categories.* FROM product" +
//            "LEFT JOIN products_categories ON product_id = product.id" +
//            "LEFT JOIN categories ON category_id = categories.id", nativeQuery = true)

    List<Product> findAllByOrderByNameAsc();

    @Query(value = "SELECT * FROM product WHERE id NOT IN " +
            "(SELECT product_id FROM products_categories)", nativeQuery = true)
    List<Product> findProductWithoutCategory();

    @Query(value = "SELECT * FROM product WHERE id IN " +
            "(SELECT product_id FROM products_categories WHERE category_id = :id) " +
            "ORDER BY name", nativeQuery = true)
    List<Product> findProductListByCategory(@Param("id") Integer categoryId);

}