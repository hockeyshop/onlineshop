package net.thumbtack.onlineshop.validate;

import org.springframework.beans.factory.annotation.Value;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@Documented
@Constraint(validatedBy = PassLenghtValidation.class)
//@Target({ElementType.FIELD, ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
public @interface PassLenght {

    String message() default "{PassLenght}";
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};
}