package net.thumbtack.onlineshop.validate;

import net.thumbtack.onlineshop.exeptions.UserErrorCode;
import net.thumbtack.onlineshop.exeptions.UserException;
import net.thumbtack.onlineshop.model.modelRequest.UserRequest;
import org.springframework.validation.BindingResult;


public class UserRequestValidation {

    public static UserRequest userRequestValidation(UserRequest userRequest, BindingResult bindingResult) {
        if (bindingResult != null) {
            if (bindingResult.hasErrors()) {
                String errorValue = "";
                if (bindingResult.getFieldError().getRejectedValue() == null) {
                    errorValue = "NULL";
                } else {
                    errorValue = bindingResult.getFieldError().getRejectedValue().toString();
                }
                throw new UserException(UserErrorCode.INCORRECT_DATE,
                        bindingResult.getFieldError().getField(),
                        errorValue + " - " + bindingResult.getFieldError().getDefaultMessage());
            }
        }
        if (userRequest.getPhone() != null){
            String phoneNumber = userRequest.getPhone();
            phoneNumber = phoneNumber.replaceAll("[\\s\\-\\+]", "");
            phoneNumber = phoneNumber.replaceFirst("7", "8");
            userRequest.setPhone(phoneNumber);
            userRequest.setDeposit(0);
        }

        return userRequest;
    }


}
