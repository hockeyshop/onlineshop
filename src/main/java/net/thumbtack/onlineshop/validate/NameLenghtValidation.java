package net.thumbtack.onlineshop.validate;

import org.springframework.beans.factory.annotation.Value;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class NameLenghtValidation implements ConstraintValidator<NameLenght, String> {
    @Value("${max_name_length}")
    private Integer MAX_NAME_LENGTH;

    @Override
    public void initialize(NameLenght constraintAnnotation) {

    }

    @Override
    public boolean isValid(String nameFild, ConstraintValidatorContext constraintValidatorContext) {
        if (nameFild != null){
            if(nameFild.length() > MAX_NAME_LENGTH) {
                return false;
            }
            return nameFild.matches("^[а-яА-Я\\s\\-]*$");
        }
        return true;
    }
}
