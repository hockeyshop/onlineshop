package net.thumbtack.onlineshop.validate;

import org.hibernate.annotations.Target;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@Documented
@Constraint(validatedBy = NameLenghtValidation.class)
//@Target({ElementType.FIELD, ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
public @interface NameLenght {
    String message() default "{NameLenght}";
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};
}
