package net.thumbtack.onlineshop.validate;

import org.springframework.beans.factory.annotation.Value;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class PassLenghtValidation implements ConstraintValidator<PassLenght, String> {

    @Value("${min_password_length}")
    private Integer MIN_PASSWORD_LENGTH;

    @Override
    public void initialize(PassLenght constraintAnnotation) {

    }

    @Override
    public boolean isValid(String password, ConstraintValidatorContext constraintValidatorContext) {
        if (password != null) {
            return password.length() > MIN_PASSWORD_LENGTH;
        }
        else {
            return true;
        }
    }
}
