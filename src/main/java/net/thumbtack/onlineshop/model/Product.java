package net.thumbtack.onlineshop.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import net.thumbtack.onlineshop.model.modelRequest.ProductRequest;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Data
@AllArgsConstructor
@Entity
@Table(name = "product")
public class Product {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    private String name;                       // "название товара"
    private Integer price;                     // "стоимость за единицу товара"
    private Integer count;                     // "количество товара"

//    @OneToMany(cascade = CascadeType.ALL)
//    private List<Integer> categoriesId;        // "категории товара"

    @ManyToMany(cascade = CascadeType.REFRESH)
    @JoinTable(name = "products_categories",
            joinColumns = @JoinColumn(name = "product_id"),
            inverseJoinColumns = @JoinColumn(name = "category_id"))
    private List<Category> categories;

    public Product() {
        this.categories = new ArrayList<>();
    }

    public Product(ProductRequest productRequest) {
        this.name = productRequest.getName();
        this.price = productRequest.getPrice();
        this.count = productRequest.getCount();
        this.categories = new ArrayList<>();
    }

    public Product(String name, Integer price, Integer count, List<Category> categories) {
        this.name = name;
        this.price = price;
        this.count = count;
        this.categories = categories;
    }

    public Product(String name, Integer price, Integer count) {
        this.name = name;
        this.price = price;
        this.count = count;
        this.categories = new ArrayList<>();
    }

}
