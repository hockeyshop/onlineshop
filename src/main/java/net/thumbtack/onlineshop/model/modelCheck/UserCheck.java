package net.thumbtack.onlineshop.model.modelCheck;

import net.thumbtack.onlineshop.exeptions.UserErrorCode;
import net.thumbtack.onlineshop.exeptions.UserException;
import net.thumbtack.onlineshop.model.modelRequest.UserRequest;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;


//   не используется!!!!!!
//   заменен на UserRequestValidation

@Service
public class UserCheck{
    @Value("${max_name_length}")
    private Integer MAX_NAME_LENGTH;

    @Value("${min_password_length}")
    private Integer MIN_PASSWORD_LENGTH;

    public UserCheck() {
    }

    public void validate (UserRequest userRequest) throws Exception {
        if (userRequest.getFirstName() == null  || userRequest.getFirstName().isEmpty()) {
            throw new UserException(UserErrorCode.FIRST_NAME_WRONG, "firstName", "firstName is NULL");
        }
        if (userRequest.getFirstName().length() > MAX_NAME_LENGTH) {
            throw new UserException(UserErrorCode.FIRST_NAME_WRONG, "firstName", "ERROR");
        }

    }


}
