package net.thumbtack.onlineshop.model.modelCheck;

import net.thumbtack.onlineshop.exeptions.UserErrorCode;
import net.thumbtack.onlineshop.exeptions.UserException;
import net.thumbtack.onlineshop.model.modelRequest.ProductRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;

@Service
public abstract class ProductCheck {

    public static void productCheck(ProductRequest productRequest) throws UserException{
        if (productRequest.getCount() == null){
            productRequest.setCount(0);
        }
        if (productRequest.getCategoriesId() == null){
            productRequest.setCategoriesId(new ArrayList<>());
        }
        if (productRequest.getPrice() == null || productRequest.getPrice() <= 0){
            throw new UserException(UserErrorCode.PRICE_NOT_CORRECT, "price", "The price field cannot be less than or equal to zero");
        }
//        if (productRequest.getName() == null || productRequest.getName().isEmpty()){
//            throw new UserException(UserErrorCode.NAME_NOT_CORRECT, "name", "NameLenght field cannot be empty");
//        }

    }
}
