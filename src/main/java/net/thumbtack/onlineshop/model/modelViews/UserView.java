package net.thumbtack.onlineshop.model.modelViews;

public class UserView {

    public interface ShortView {}

    public interface ClientShortView extends ShortView{}

    public interface ClientFullView extends ClientShortView{}

    public interface AdminView extends ShortView{}

    public interface FullView extends ShortView {}
}
