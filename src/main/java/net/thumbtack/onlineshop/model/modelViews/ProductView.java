package net.thumbtack.onlineshop.model.modelViews;

public class ProductView {

    public interface ProductShortView{}

    public interface ProductNameView extends ProductShortView{}

    public interface ProductIdView extends ProductShortView{}
}
