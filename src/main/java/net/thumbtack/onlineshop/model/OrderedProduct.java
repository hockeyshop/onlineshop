package net.thumbtack.onlineshop.model;

import javax.persistence.*;
import java.util.Objects;

@Entity
public class OrderedProduct {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;
    private Integer count;  // заказанное количество товара

    @OneToOne
    private Product product;

    public OrderedProduct() {
    }

    public OrderedProduct(Product product, Integer count) {
        this.count = count;
        this.product = product;
    }

    @Override
    public String toString() {
        return "OrderedProduct{" +
                "id=" + id +
                ", count=" + count +
                ", product=" + product +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof OrderedProduct)) return false;
        OrderedProduct that = (OrderedProduct) o;
        return Objects.equals(id, that.id) &&
                Objects.equals(count, that.count) &&
                Objects.equals(product, that.product);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, count, product);
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }
}
