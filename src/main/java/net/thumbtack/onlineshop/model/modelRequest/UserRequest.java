package net.thumbtack.onlineshop.model.modelRequest;

import lombok.AllArgsConstructor;
import lombok.Data;
import net.thumbtack.onlineshop.validate.NameLenght;
import net.thumbtack.onlineshop.validate.PassLenght;
import org.springframework.stereotype.Component;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

@Component
@Data
@AllArgsConstructor
public class UserRequest {

    @NameLenght
    @NotNull
    @Pattern(regexp = "^[а-яА-Я\\s\\-]*$")
    private String firstName;      // "имя"
    @NameLenght
    @NotNull
    @Pattern(regexp = "^[а-яА-Я\\s\\-]*$")
    private String lastName;       // "фамилия"
    @NameLenght
    @Pattern(regexp = "^[а-яА-Я\\s\\-]*$")
    private String patronymic;     // "отчество" необязателен
//    @NameLenght
    private String position;       // "должность"
    @Email
    private String email;          // "email"
    private String address;        // "почтовый адрес"
    @Pattern(regexp = "(\\+7|8)[- _]*\\(?[- _]*(\\d{3}[- _]*\\)?([- _]*\\d){7}|\\d\\d[- _]*\\d\\d[- _]*\\)?([- _]*\\d){6})")
    private String phone;          // "номер телефона"
//    private Integer deposit;     // "денежный счет" при создании нового пользователя не задается
    @NotNull(message = "Login field cannot be empty")
    @Pattern(regexp="^[а-яА-ЯёЁa-zA-Z0-9]+$", message = "Неверный логин")
    private String login;          // "логин"
    @NotNull
    @PassLenght
    private String password;       // "пароль"
    @PassLenght
    private String newPassword;    // новый пароль

    public UserRequest() {
    }

    public UserRequest(String firstName, String lastName, String patronymic, String email, String address, String phone, String login, String password) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.patronymic = patronymic;
        this.email = email;
        this.address = address;
        this.phone = phone;
        this.login = login;
        this.password = password;
    }

    public UserRequest(String firstName, String lastName, String patronymic, String position, String login, String password) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.patronymic = patronymic;
        this.position = position;
        this.login = login;
        this.password = password;
    }

    public void setDeposit(Integer i) {

    }
}
