package net.thumbtack.onlineshop.model.modelRequest;

import lombok.AllArgsConstructor;
import lombok.Data;
import org.springframework.stereotype.Component;

@Component
@Data
@AllArgsConstructor
public class LoginUserRequest {
    private String login;
    private String password;

    public LoginUserRequest() {
    }


}
