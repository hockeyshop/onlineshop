package net.thumbtack.onlineshop.model.modelRequest;

import lombok.AllArgsConstructor;
import lombok.Data;
import org.springframework.stereotype.Component;

import javax.validation.constraints.NotNull;

@Component
@Data
@AllArgsConstructor
public class PurchaseRequest {
    @NotNull
    private  Integer productId;    // "идентификатор товара"
    @NotNull
    private  String productName;   // "название товара"
    @NotNull
    private  Integer productPrice; // стоимость за единицу товара --- зачем в запросе на покупку
    @NotNull
    private  Integer productCount; // количество единиц товара

    public PurchaseRequest() {
    }
}
