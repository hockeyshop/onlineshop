package net.thumbtack.onlineshop.model.modelRequest;


import lombok.Data;
import org.springframework.stereotype.Component;

@Component
@Data
public class CategoryRequest {
    private String categoryName;
    private Integer parentId;

    public CategoryRequest() {
    }

    public CategoryRequest(String categoryName) {
        this.categoryName = categoryName;
//        this.setParentId(0);
    }

    public CategoryRequest(Integer parentId) {
//        this.categoryName = "";
        this.parentId = parentId;
    }

    public CategoryRequest(String categoryName, Integer parentId) {
        this.categoryName = categoryName;
        this.parentId = parentId;
    }
}
