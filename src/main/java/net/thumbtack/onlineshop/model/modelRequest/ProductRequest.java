package net.thumbtack.onlineshop.model.modelRequest;

import lombok.AllArgsConstructor;
import lombok.Data;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@Data
@AllArgsConstructor
public class ProductRequest {
    private String name;
    private Integer price;
    private Integer count;
    private List<Integer> categoriesId;

    public ProductRequest() {
    }

    public ProductRequest(String name, Integer price, Integer count) {
        this.name = name;
        this.price = price;
        this.count = count;
    }

}
