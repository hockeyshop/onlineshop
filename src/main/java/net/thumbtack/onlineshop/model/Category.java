package net.thumbtack.onlineshop.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import net.thumbtack.onlineshop.model.modelRequest.CategoryRequest;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Data
@AllArgsConstructor
@Table(name = "categories")
public class Category {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;
    private String name;      // "название категории"

    private Integer parentCategoryId;  // ID родительской категории

    @ManyToMany//(mappedBy = "categories")
    @JoinTable(name = "products_categories",
            joinColumns = @JoinColumn(name = "category_id"),
            inverseJoinColumns = @JoinColumn(name = "product_id"))
    private List<Product> products;

    public Category() {
    }

    public Category(String name) {
        this.name = name;
        this.parentCategoryId = null;
        this.products = new ArrayList<>();
    }

    public Category(String subCategoryName, Integer parentId) {
        this.name = subCategoryName;
        this.parentCategoryId = parentId;
        this.products = new ArrayList<>();
    }


    public Category(CategoryRequest categoryRequest) {
        this.name = categoryRequest.getCategoryName();
        this.parentCategoryId = categoryRequest.getParentId();
        this.products = new ArrayList<>();
    }
}
