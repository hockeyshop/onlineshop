package net.thumbtack.onlineshop.model;

import com.fasterxml.jackson.annotation.JsonView;
import lombok.AllArgsConstructor;
import lombok.Data;
import net.thumbtack.onlineshop.model.modelRequest.UserRequest;
import net.thumbtack.onlineshop.model.modelViews.UserView;
import net.thumbtack.onlineshop.security.Role;
import org.hibernate.annotations.Proxy;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.*;
import javax.validation.constraints.Email;
import java.util.*;

@Data
@AllArgsConstructor
@Entity
@Table(name = "user")
@Proxy(lazy = false)
public class User implements UserDetails {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @JsonView(UserView.ShortView.class)
    private Integer id;
    @JsonView(UserView.ShortView.class)
    private String firstName;      // "имя"
    @JsonView(UserView.ShortView.class)
    private String lastName;       // "фамилия"
    @JsonView(UserView.ShortView.class)
    private String patronymic;     // "отчество" необязателен
    @JsonView(UserView.AdminView.class)
    private String position;       // "должность"
    @JsonView(UserView.ClientShortView.class)
    private String email;          // "email"
    @JsonView(UserView.ClientShortView.class)
    private String address;        // "почтовый адрес"
    @JsonView(UserView.ClientShortView.class)
    private String phone;          // "номер телефона"
    @JsonView(UserView.ClientFullView.class)
    private Integer deposit;       // "денежный счет"
    @JsonView(UserView.FullView.class)
    private String login;          // "логин"
    @JsonView(UserView.FullView.class)
    private String password;       // "пароль"

//    @OneToMany(cascade = CascadeType.ALL)
//    @JsonView(UserView.FullView.class)
//    private List<OrderedProduct> userBasket;

    @ElementCollection
    private Map<Product, Integer> basket;

//    private String username;

    @Transient
    private String passwordConfirm;
    @ManyToMany(fetch = FetchType.EAGER)
    private Set<Role> roles = new HashSet<>();


    public User() {
    }

    public User(UserRequest request) {
        this.firstName = request.getFirstName();
        this.lastName = request.getLastName();
        this.patronymic = request.getPatronymic();
        this.position = request.getPosition();
        this.email = request.getEmail();
        this.address = request.getAddress();
        this.phone = request.getPhone();
        this.login = request.getLogin();
        this.password = request.getPassword();
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return getRoles();
    }

    @Override
    public String getUsername() {
        return this.login;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }

    public String getPasswordConfirm() {
        return passwordConfirm;
    }

    public void setPasswordConfirm(String passwordConfirm) {
        this.passwordConfirm = passwordConfirm;
    }
}
