package net.thumbtack.onlineshop.model.modelResponse;

import lombok.AllArgsConstructor;
import lombok.Data;
import net.thumbtack.onlineshop.model.Product;
import net.thumbtack.onlineshop.model.modelRequest.PurchaseRequest;
import org.springframework.stereotype.Component;

import javax.swing.*;

@Component
@Data
@AllArgsConstructor
public class PurchaseResponse {
    private  Integer productId;    // "идентификатор товара"
    private  String productName;   // "название товара"
    private  Integer productPrice; // стоимость за единицу товара --- зачем в запросе на покупку
    private  Integer productCount; // количество единиц товара

    public PurchaseResponse() {
    }

    public PurchaseResponse(PurchaseRequest purchaseRequest) {
        this.productId = purchaseRequest.getProductId();
        this.productName = purchaseRequest.getProductName();
        this.productPrice = purchaseRequest.getProductPrice();
        this.productCount = purchaseRequest.getProductCount();
    }

    public PurchaseResponse(Product product, Integer productCount) {
        this.productId = product.getId();
        this.productName = product.getName();
        this.productPrice = product.getPrice();
        this.productCount = productCount;
    }
}

