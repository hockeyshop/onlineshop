package net.thumbtack.onlineshop.model.modelResponse;

import lombok.Data;
import net.thumbtack.onlineshop.model.Category;
import org.springframework.stereotype.Component;

@Component
@Data
public class SubCategoryResponse extends CategoryResponse {
    private Integer parentId;
    private String parentName;


    public SubCategoryResponse(Integer id, String name, Integer parentId, String parentName) {
        super(id, name);
        this.parentId = parentId;
        this.parentName = parentName;
    }

    public SubCategoryResponse() {
    }

    public SubCategoryResponse(Category category, String parentName) {
        super(category.getId(), category.getName());
        this.parentId = category.getParentCategoryId();
        this.parentName = parentName;
    }

}
