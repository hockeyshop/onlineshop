package net.thumbtack.onlineshop.model.modelResponse;

import com.fasterxml.jackson.annotation.JsonView;
import lombok.AllArgsConstructor;
import lombok.Data;
import net.thumbtack.onlineshop.model.Product;
import net.thumbtack.onlineshop.model.modelViews.ProductView;
import net.thumbtack.onlineshop.model.modelViews.UserView;
import org.springframework.stereotype.Component;

import java.util.*;

@Component
@Data
@AllArgsConstructor
public class ProductResponse {
    @JsonView(ProductView.ProductShortView.class)
    private Integer id;
    @JsonView(ProductView.ProductShortView.class)
    private String name;
    @JsonView(ProductView.ProductShortView.class)
    private Integer price;
    @JsonView(ProductView.ProductShortView.class)
    private Integer count;
    @JsonView(ProductView.ProductIdView.class)
    private List<Integer> categoriesId;
    @JsonView(ProductView.ProductNameView.class)
    private List<String> categoriesNames;

    public ProductResponse() {
    }

    public ProductResponse(Product product) {
        this.id = product.getId();
        this.name = product.getName();
        this.price = product.getPrice();
        this.count = product.getCount();
        this.categoriesId = new ArrayList<>();
        this.categoriesNames = new ArrayList<>();
    }

    public ProductResponse(Product product, List<Integer> categoriesId, List<String> categoriesNames) {
        this.id = product.getId();
        this.name = product.getName();
        this.price = product.getPrice();
        this.count = product.getCount();
        this.categoriesId = categoriesId;
        this.categoriesNames = categoriesNames;
    }

//    public ProductResponse(Product product, List<Integer> categoriesId) {
//        this.id = product.getId();
//        this.name = product.getName();
//        this.price = product.getPrice();
//        this.count = product.getCount();
//        this.categoriesId = categoriesId;
//        this.categoriesNames = new ArrayList<>();
//    }

//    public ProductResponse(Product product, List<String> categoriesNames) {
//        this.id = product.getId();
//        this.name = product.getName();
//        this.price = product.getPrice();
//        this.count = product.getCount();
//        this.categoriesId = new ArrayList<>();
//        this.categoriesNames = categoriesNames;
//    }


}
