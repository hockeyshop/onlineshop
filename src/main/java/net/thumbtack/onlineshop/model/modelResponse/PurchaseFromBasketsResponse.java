package net.thumbtack.onlineshop.model.modelResponse;

import lombok.AllArgsConstructor;
import lombok.Data;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@Data
@AllArgsConstructor
public class PurchaseFromBasketsResponse {
    private List<PurchaseResponse> bought;
    private List<PurchaseResponse> remaining;

    public PurchaseFromBasketsResponse() {
    }
}
