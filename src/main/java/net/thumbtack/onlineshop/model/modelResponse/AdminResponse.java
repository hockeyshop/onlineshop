package net.thumbtack.onlineshop.model.modelResponse;

import lombok.AllArgsConstructor;
import lombok.Data;
import net.thumbtack.onlineshop.model.User;
import org.springframework.stereotype.Component;

@Component
@Data
@AllArgsConstructor
public class AdminResponse extends UserResponse {
    private Integer id;
    private String firstName;      // "имя"
    private String lastName;       // "фамилия"
    private String patronymic;     // "отчество" необязателен
    private String position;          // "email"

    public AdminResponse() {
    }

    public AdminResponse(User user) {
        this.id = user.getId();
        this.firstName = user.getFirstName();
        this.lastName = user.getLastName();
        this.patronymic = user.getPatronymic();
        this.position = user.getPosition();
    }
}
