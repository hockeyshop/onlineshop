package net.thumbtack.onlineshop.model.modelResponse;

import lombok.AllArgsConstructor;
import lombok.Data;
import net.thumbtack.onlineshop.model.User;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

@Component
@Data
@AllArgsConstructor
public class ClientResponse extends UserResponse {
    private Integer id;
    private String firstName;      // "имя"
    private String lastName;       // "фамилия"
    private String patronymic;     // "отчество" необязателен
    private String email;          // "email"
    private String address;        // "почтовый адрес"
    private String phone;          // "номер телефона"
    private Integer deposit;       // "денежный счет"

//    @Bean
//    public UserResponse userResponse(){
//        return new UserResponse();
//    }

    public ClientResponse() {
    }

    public ClientResponse(User user) {
        this.id = user.getId();
        this.firstName = user.getFirstName();
        this.lastName = user.getLastName();
        this.patronymic = user.getPatronymic();
        this.email = user.getEmail();
        this.address = user.getAddress();
        this.phone = user.getPhone();
        this.deposit = user.getDeposit();
    }
}
