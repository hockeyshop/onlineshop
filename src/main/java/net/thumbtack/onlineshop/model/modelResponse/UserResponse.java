package net.thumbtack.onlineshop.model.modelResponse;

import com.fasterxml.jackson.annotation.JsonView;
import lombok.AllArgsConstructor;
import lombok.Data;
import net.thumbtack.onlineshop.model.OrderedProduct;
import net.thumbtack.onlineshop.model.User;
import net.thumbtack.onlineshop.model.modelViews.UserView;
import org.springframework.stereotype.Component;

import javax.persistence.CascadeType;
import javax.persistence.OneToMany;
import javax.validation.constraints.Email;
import java.util.List;

@Component
@Data
@AllArgsConstructor
public class UserResponse {
    private Integer id;
    private String firstName;      // "имя"
    private String lastName;       // "фамилия"
    private String patronymic;     // "отчество" необязателен

    public UserResponse() {
    }

    public UserResponse(String firstName, String lastName, String patronymic) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.patronymic = patronymic;
    }

    public UserResponse(User user) {
        this.id = user.getId();
        this.firstName = user.getFirstName();
        this.lastName = user.getLastName();
        this.patronymic = user.getPatronymic();
    }
}
