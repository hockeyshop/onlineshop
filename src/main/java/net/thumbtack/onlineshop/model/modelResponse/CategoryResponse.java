package net.thumbtack.onlineshop.model.modelResponse;

import lombok.Data;
import net.thumbtack.onlineshop.model.Category;
import org.springframework.stereotype.Component;

@Component
@Data
public class CategoryResponse {
    private Integer id;
    private String categoryName;

    public CategoryResponse() {
    }

    public CategoryResponse(Integer id, String categoryName) {
        this.id = id;
        this.categoryName = categoryName;
    }

    public CategoryResponse(Category category) {
        this.id = category.getId();
        this.categoryName = category.getName();
    }
}
