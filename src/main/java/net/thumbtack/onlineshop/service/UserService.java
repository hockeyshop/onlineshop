package net.thumbtack.onlineshop.service;

import net.thumbtack.onlineshop.model.modelRequest.PurchaseRequest;
import net.thumbtack.onlineshop.model.modelResponse.PurchaseFromBasketsResponse;
import net.thumbtack.onlineshop.model.modelResponse.PurchaseResponse;
import net.thumbtack.onlineshop.model.modelResponse.UserResponse;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface UserService {


    UserResponse depMoney(Integer userId, Integer deposit);

    UserResponse getMoney(Integer userId);

    PurchaseResponse purchase(PurchaseRequest purchaseRequest, Integer userId);

    List<PurchaseResponse> addProductInBaskets(PurchaseRequest purchaseRequest, Integer userId);

    String deleteProductInBaskets(Integer userId, Integer productId);

    List<PurchaseResponse> updateProductInBaskets(PurchaseRequest purchaseRequest, Integer userId);

    List<PurchaseResponse> getUserBaskets(Integer userId);

    PurchaseFromBasketsResponse purchaseFromBaskets(List<PurchaseRequest> purchaseRequestList, Integer userId);
}
