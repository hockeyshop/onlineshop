package net.thumbtack.onlineshop.service;

import net.thumbtack.onlineshop.exeptions.UserException;
import net.thumbtack.onlineshop.model.Category;
import net.thumbtack.onlineshop.model.modelRequest.CategoryRequest;
import net.thumbtack.onlineshop.model.modelResponse.CategoryResponse;
import net.thumbtack.onlineshop.model.modelResponse.SubCategoryResponse;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface CategoryService {

    CategoryResponse addCategory(String categoryName);

    SubCategoryResponse addSubCategory(String subCategoryName, Integer parentId);

    CategoryResponse getCategoryByNumber(Integer num);

    Category getCategoryById(Integer id);

    List<CategoryResponse> getListCategories();

    List<Category> findAllCategory();

    String delCategoryById(Integer id);

    void delCategoryAll();

    CategoryResponse editCategoryByNumber(Integer num, CategoryRequest categoryRequest) throws UserException;

}
