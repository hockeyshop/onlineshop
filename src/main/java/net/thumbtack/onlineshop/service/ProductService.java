package net.thumbtack.onlineshop.service;

import net.thumbtack.onlineshop.model.modelRequest.ProductRequest;
import net.thumbtack.onlineshop.model.modelResponse.ProductResponse;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface ProductService {

    ProductResponse addProduct(ProductRequest productRequest);

    ProductResponse updateProduct(Integer id, ProductRequest productRequest);

    String deleteProduct(Integer id);

    ProductResponse infoProduct(Integer id);

    List<ProductResponse> infoProductList(List<Integer> categories, String order);

    List<ProductResponse> addAllProducts(List<ProductRequest> productRequest);
}
