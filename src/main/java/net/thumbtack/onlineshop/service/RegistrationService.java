package net.thumbtack.onlineshop.service;

import net.thumbtack.onlineshop.exeptions.UserException;
import net.thumbtack.onlineshop.model.User;
import net.thumbtack.onlineshop.model.modelRequest.UserRequest;
import net.thumbtack.onlineshop.model.modelResponse.UserResponse;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface RegistrationService {

    UserResponse saveAdmin(UserRequest request) throws UserException;

    UserResponse saveUser(UserRequest request) throws UserException;

    UserResponse updateUser(UserRequest userRequest, Integer userId);

    UserResponse updateAdmin(UserRequest userRequest, Integer userId);

//    User findUserByLoginAndPassword(String login, String password);

    UserResponse findUserById(Integer id);

    User getUserById(Integer id) throws UserException;

    void userDeleteById(Integer id);

    UserResponse userLogin(String login, String password);

    List<UserResponse> findAllUsers();

    List<User> saveAllUsers(List<User> users);

    List<User> saveAllAdmins(List<User> users);

//    User findUserByLogin(String login);

}
