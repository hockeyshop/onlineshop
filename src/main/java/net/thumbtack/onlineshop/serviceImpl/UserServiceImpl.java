package net.thumbtack.onlineshop.serviceImpl;

import net.thumbtack.onlineshop.exeptions.UserErrorCode;
import net.thumbtack.onlineshop.exeptions.UserException;
import net.thumbtack.onlineshop.model.Product;
import net.thumbtack.onlineshop.model.User;
import net.thumbtack.onlineshop.model.modelRequest.PurchaseRequest;
import net.thumbtack.onlineshop.model.modelResponse.ProductResponse;
import net.thumbtack.onlineshop.model.modelResponse.PurchaseFromBasketsResponse;
import net.thumbtack.onlineshop.model.modelResponse.PurchaseResponse;
import net.thumbtack.onlineshop.model.modelResponse.UserResponse;
import net.thumbtack.onlineshop.repository.OrderedProductRepository;
import net.thumbtack.onlineshop.repository.ProductRepository;
import net.thumbtack.onlineshop.repository.RoleRepository;
import net.thumbtack.onlineshop.repository.UserRepository;
import net.thumbtack.onlineshop.service.RegistrationService;
import net.thumbtack.onlineshop.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Service
public class UserServiceImpl implements UserService {
    @Autowired
    private RegistrationService registrationService;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private RoleRepository roleRepository;
    @Autowired
    private ProductRepository productRepository;
    @Autowired
    private OrderedProductRepository orderedProductRepository;
//    @Autowired
//    BCryptPasswordEncoder bCryptPasswordEncoder;

    public UserServiceImpl() {
    }


    // внесение денег на счет
    @Override
    public UserResponse depMoney(Integer userId, Integer deposit) {
        User user = userRepository.getOne(userId);
        user.setDeposit(deposit);
        return new UserResponse(userRepository.saveAndFlush(user));
    }

    // получение суммы денег, имеющейся на счете
    @Override
    public UserResponse getMoney(Integer userId) {
        return new UserResponse(userRepository.getOne(userId));
    }

    // покупка товара
    @Override
    public PurchaseResponse purchase(PurchaseRequest purchaseRequest, Integer userId) {
        Product product = productRepository.getOne(purchaseRequest.getProductId());
        User user = userRepository.getOne(userId);

        if (!purchaseRequest.getProductName().equals(product.getName())) {
            throw new UserException(UserErrorCode.INCORRECT_DATE, "productName",
                    "Наименование товара  \"" + purchaseRequest.getProductName() + "\"  не соответсвует существующему");
        }
        if (!purchaseRequest.getProductPrice().equals(product.getPrice())) {
            throw new UserException(UserErrorCode.INCORRECT_DATE, "productPrice",
                    "Стоимость товара  \"" + purchaseRequest.getProductName() + "\"  не соответсвует существующей");
        }
        if (purchaseRequest.getProductPrice() * purchaseRequest.getProductCount() < user.getDeposit()) {
            throw new UserException(UserErrorCode.INCORRECT_DATE, "productCount",
                    "Суммарная стоимость всех единиц товара  \"" + purchaseRequest.getProductName() + "\"  превышает количество денег на счете");
        }
        if (purchaseRequest.getProductCount() > product.getCount()) {
            throw new UserException(UserErrorCode.INCORRECT_DATE, "productCount",
                    "Не имеется требуемое количество единиц товара  \"" + purchaseRequest.getProductName() + "\"");
        }
        product.setCount(product.getCount() - purchaseRequest.getProductCount());
        productRepository.save(product);
        user.getBasket().remove(product);
        userRepository.save(user);
        return new PurchaseResponse(purchaseRequest);
    }

    // добавление товара в корзину
    @Override
    public List<PurchaseResponse> addProductInBaskets(PurchaseRequest purchaseRequest, Integer userId) {
        Product product = productRepository.getOne(purchaseRequest.getProductId());
        User user = userRepository.getOne(userId);

        if (!purchaseRequest.getProductName().equals(product.getName())) {
            throw new UserException(UserErrorCode.INCORRECT_DATE, "productName", "Имя продукта не соответсвует существующему");
        }
        if (!purchaseRequest.getProductPrice().equals(product.getPrice())) {
            throw new UserException(UserErrorCode.INCORRECT_DATE, "productPrice", "Стоимость продукта не соответсвует существующей");
        }
//        OrderedProduct orderedProduct = new OrderedProduct(product, purchaseRequest.getProductCount());
//        user.getUserBasket().add(orderedProductRepository.saveAndFlush(orderedProduct));
        user.getBasket().put(product, purchaseRequest.getProductCount());
        userRepository.saveAndFlush(user);
        List<PurchaseResponse> purchaseResponseList = new ArrayList<>();
        for (Product prod : user.getBasket().keySet()) {
            purchaseResponseList.add(new PurchaseResponse(prod, user.getBasket().get(prod)));
        }
        return purchaseResponseList;
    }

    // удаление товара из корзины
    @Override
    public String deleteProductInBaskets(Integer userId, Integer productId) {
        User user = userRepository.getOne(userId);
        Product product = productRepository.getOne(productId);
        user.getBasket().remove(product);
        userRepository.saveAndFlush(user);
        return "";
    }

    // изменение количества товара в корзине
    @Override
    public List<PurchaseResponse> updateProductInBaskets(PurchaseRequest purchaseRequest, Integer userId) {
        User user = userRepository.getOne(userId);

        if (productRepository.findById(purchaseRequest.getProductId()).isPresent()) {
            Product product = productRepository.getOne(purchaseRequest.getProductId());
            if (!purchaseRequest.getProductName().equals(product.getName())) {
                throw new UserException(UserErrorCode.INCORRECT_DATE, "productName", "Имя продукта не соответсвует существующему");
            }
            if (!purchaseRequest.getProductPrice().equals(product.getPrice())) {
                throw new UserException(UserErrorCode.INCORRECT_DATE, "productPrice", "Стоимость продукта не соответсвует существующей");
            }
            user.getBasket().put(product, purchaseRequest.getProductCount());
        } else {
            for (Product prod : user.getBasket().keySet()) {
                if (prod.getId().equals(purchaseRequest.getProductId())) {
                    if (!purchaseRequest.getProductName().equals(prod.getName())) {
                        throw new UserException(UserErrorCode.INCORRECT_DATE, "productName", "Имя продукта не соответсвует существующему");
                    }
                    if (!purchaseRequest.getProductPrice().equals(prod.getPrice())) {
                        throw new UserException(UserErrorCode.INCORRECT_DATE, "productPrice", "Стоимость продукта не соответсвует существующей");
                    }
                    user.getBasket().put(prod, purchaseRequest.getProductCount());
                }
            }
        }

        userRepository.saveAndFlush(user);

        List<PurchaseResponse> purchaseResponseList = new ArrayList<>();
        for (Product prod : user.getBasket().keySet()) {
            purchaseResponseList.add(new PurchaseResponse(prod, user.getBasket().get(prod)));
        }
        return purchaseResponseList;
    }

    // получение состава корзины
    @Override
    public List<PurchaseResponse> getUserBaskets(Integer userId) {
        List<PurchaseResponse> purchaseResponseList = new ArrayList<>();
        User user = userRepository.getOne(userId);
        for (Product prod : user.getBasket().keySet()) {
            purchaseResponseList.add(new PurchaseResponse(prod, user.getBasket().get(prod)));
        }
        return purchaseResponseList;
    }

    // покупка товаров из корзины
    @Override
    @Transactional
    public PurchaseFromBasketsResponse purchaseFromBaskets(List<PurchaseRequest> purchaseRequestList, Integer userId) {
        List<PurchaseResponse> boughtList = new ArrayList<>();
        List<PurchaseResponse> remainingList = new ArrayList<>();
        Product product = new Product();
        Integer totalCost = 0;
        User user = userRepository.getOne(userId);

        for (PurchaseRequest purchaseRequest : purchaseRequestList) {
            if (productRepository.findById(purchaseRequest.getProductId()).isPresent()){
                if (purchaseRequest.getProductCount() == null ||
                        purchaseRequest.getProductCount() > user.getBasket().get(product)){
                    purchaseRequest.setProductCount(user.getBasket().get(product));
                }
                try {
                    boughtList.add(purchase(purchaseRequest, userId));
                    totalCost = totalCost + (purchaseRequest.getProductPrice()*purchaseRequest.getProductCount());
                } catch (UserException ex) {
                    remainingList.add(new PurchaseResponse(purchaseRequest));
                }
            }
            else {
                remainingList.add(new PurchaseResponse(purchaseRequest));
            }
        }
        if (totalCost > user.getDeposit()){
            throw new UserException(UserErrorCode.REQUEST_WRONG, "Total cost", "Общая стоимость товаров больше, чем средств на депозите");
        }
        userRepository.flush();
        productRepository.flush();
        return new PurchaseFromBasketsResponse(boughtList, remainingList);
    }

}
