package net.thumbtack.onlineshop.serviceImpl;

import net.thumbtack.onlineshop.exeptions.UserErrorCode;
import net.thumbtack.onlineshop.exeptions.UserException;
import net.thumbtack.onlineshop.model.Category;
import net.thumbtack.onlineshop.model.modelRequest.CategoryRequest;
import net.thumbtack.onlineshop.model.modelResponse.CategoryResponse;
import net.thumbtack.onlineshop.model.modelResponse.SubCategoryResponse;
import net.thumbtack.onlineshop.repository.CategoryRepository;
import net.thumbtack.onlineshop.service.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

@Service
public class CategoryServiceImpl implements CategoryService {
    @Autowired
    private CategoryRepository categoryRepository;


    // добавление категории
    @Override
    public CategoryResponse addCategory(String categoryName) {
        Category category = categoryRepository.saveAndFlush(new Category(categoryName));
        return new CategoryResponse(category.getId(), category.getName());
    }

    // добавление подкатегории
    @Override
    public SubCategoryResponse addSubCategory(String subCategoryName, Integer parentId) {
        Category category = categoryRepository.saveAndFlush(new Category(subCategoryName, parentId));
        Category parentCategory = categoryRepository.getOne(parentId);
        SubCategoryResponse response = new SubCategoryResponse(category, parentCategory.getName());

        return response;
    }

    // получение категории по её номеру
    @Override
    public CategoryResponse getCategoryByNumber(Integer num) {
        return getListCategories().get(num);
    }

    // получение категории по её Id
    @Override
    public Category getCategoryById(Integer id) {
        return categoryRepository.getOne(id);
    }

    //  редактирование категории (подкатегории) с номером {num}
    @Override
    public CategoryResponse editCategoryByNumber(Integer num, CategoryRequest categoryRequest) throws UserException {
        CategoryResponse categoryResponse;
        Category category = categoryRepository.getOne(getCategoryByNumber(num).getId());

        if (categoryRequest.getCategoryName() == null && categoryRequest.getParentId() == null) {
            throw new UserException(UserErrorCode.REQUEST_WRONG, "name, parentID", "At least one of the request fields must not be empty.");
        }

        if (categoryRequest.getCategoryName() != null) {
            category.setName(categoryRequest.getCategoryName());
        }

        if (category.getParentCategoryId() == null) {
            categoryResponse = new CategoryResponse(category);
            if (categoryRequest.getParentId() != null) {
                throw new UserException(UserErrorCode.REQUEST_WRONG, "parentID", "For parent category fild ID will be NULL");
            }
        } else {
            if (categoryRequest.getParentId() != null) {
                category.setParentCategoryId(categoryRequest.getParentId());
            }
            categoryResponse = new SubCategoryResponse(category, categoryRepository.getOne(category.getParentCategoryId()).getName());
        }
        categoryRepository.saveAndFlush(category);
        return categoryResponse;
    }

    // удаление категории (подкатегории) по её ID
    @Override
    public String delCategoryById(Integer id) {
        List<Category> subCategoryList = categoryRepository.findSubCategories(id);
        for (Category category:subCategoryList){
            category.setParentCategoryId(null);
        }
        categoryRepository.deleteById(id);
        return "";
    }

    // удаление всех категорий и подкатегорий
    @Override
    public void delCategoryAll() {
        categoryRepository.deleteAll();
    }

    // получение списка категорий с их подкатегориями
    @Override
    public List<CategoryResponse> getListCategories() {
        List<CategoryResponse> categoryResponseList = new ArrayList<>();
        List<Category> subCategories = new ArrayList<>();

        List<Category> categories = categoryRepository.findParentCategories();
//        categories.sort(Comparator.comparing(Category::getName));
        for (Category category : categories) {
            categoryResponseList.add(new CategoryResponse(category));
            subCategories = categoryRepository.findSubCategories(category.getId());
            for (Category subCategory : subCategories) {
                categoryResponseList.add(new SubCategoryResponse(subCategory, category.getName()));
            }
        }
        return categoryResponseList;
    }

    // получение списка категорий сортированного по имени
    @Override
    public List<Category> findAllCategory(){
        return categoryRepository.findAllByOrderByName();
    }

}
