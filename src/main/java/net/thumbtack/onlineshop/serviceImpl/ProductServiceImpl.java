package net.thumbtack.onlineshop.serviceImpl;

import net.thumbtack.onlineshop.model.Category;
import net.thumbtack.onlineshop.model.Product;
import net.thumbtack.onlineshop.model.modelRequest.ProductRequest;
import net.thumbtack.onlineshop.model.modelResponse.ProductResponse;
import net.thumbtack.onlineshop.repository.ProductRepository;
import net.thumbtack.onlineshop.service.CategoryService;
import net.thumbtack.onlineshop.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

import static net.thumbtack.onlineshop.model.modelCheck.ProductCheck.productCheck;

@Service
public class ProductServiceImpl implements ProductService {
    @Autowired
    private ProductRepository productRepository;
    @Autowired
    private CategoryService categoryService;

    // добавление нового товара
    @Override
    public ProductResponse addProduct(ProductRequest productRequest) {
        productCheck(productRequest);
        Product product = new Product(productRequest);
        if (productRequest.getCategoriesId() != null) {
            for (Integer id : productRequest.getCategoriesId()) {
                product.getCategories().add(categoryService.getCategoryById(id));
            }
        } else {
            product.setCategories(new ArrayList<>());
        }

        List<Integer> categoriesId = new ArrayList<>();

        for (Category cat : product.getCategories()) {
            categoriesId.add(cat.getId());
        }
        ProductResponse productResponse = new ProductResponse(productRepository.saveAndFlush(product));
        productResponse.setCategoriesId(categoriesId);
        return productResponse;
    }

    // редактирование товара
    @Override
    public ProductResponse updateProduct(Integer id, ProductRequest productRequest) {
//        productCheck(productRequest);
        Product product = productRepository.getOne(id);
        List<Integer> categoriesId = new ArrayList<>();
        List<Category> categories = new ArrayList<>();
        if (productRequest.getName() != null) {
            product.setName(productRequest.getName());
        }
        if (productRequest.getPrice() != null) {
            product.setPrice(productRequest.getPrice());
        }
        if (productRequest.getCount() != null) {
            product.setCount(productRequest.getCount());
        }
        if (productRequest.getCategoriesId() != null) {
            if (productRequest.getCategoriesId().size() == 0) {
                product.setCategories(new ArrayList<>());
            } else {
                for (Integer categoryId : productRequest.getCategoriesId()) {
                    Category category = categoryService.getCategoryById(categoryId);
                    product.getCategories().add(category);
                    categoriesId.add(category.getId());
                }
            }
        }
        ProductResponse productResponse = new ProductResponse(productRepository.saveAndFlush(product));
        productResponse.setCategoriesId(categoriesId);
        return productResponse;
    }

    // удаление товара
    @Override
    public String deleteProduct(Integer id) {
        productRepository.deleteById(id);
        return "ok";
    }

    // получение информации о товаре
    @Override
    public ProductResponse infoProduct(Integer id) {
        Product product = productRepository.getOne(id);
        List<String> categoriesNames = new ArrayList<>();
        for (Category category : product.getCategories()) {
            categoriesNames.add(category.getName());
        }
        ProductResponse productResponse = new ProductResponse(productRepository.saveAndFlush(product));
        productResponse.setCategoriesNames(categoriesNames);
        return productResponse;
    }

    // получение списка товаров
    @Override
    public List<ProductResponse> infoProductList(List<Integer> categories, String order) {
        List<Product> productList = new ArrayList<>();
        ProductResponse productResponse = new ProductResponse();
        List<String> categoriesName = new ArrayList<>();
        List<ProductResponse> productResponseList = new ArrayList<>();

        if (categories == null) {
            switch (order) {
                case "product": {
                    productList = productRepository.findAllByOrderByNameAsc();
                    for (Product product : productList) {
                        if (product.getCategories() == null) product.setCategories(new ArrayList<>());
                        productResponse = new ProductResponse(product);
                        for (Category category : product.getCategories()) {
                            productResponse.getCategoriesNames().add(category.getName());
                        }

                        productResponseList.add(productResponse);
                    }
                    break;
                }
                case "category": {
                    List<Category> categoryList = categoryService.findAllCategory();
                    productList = productRepository.findProductWithoutCategory();

                    for (Product product : productList) {
                        productResponseList.add(new ProductResponse(product));
                    }
                    for (Category category : categoryList) {
                        productList.clear();
                        productList.addAll(productRepository.findProductListByCategory(category.getId()));
                        for (Product product : productList) {
                            ProductResponse prodResponse = new ProductResponse(product);
                            prodResponse.setCategoriesNames(Collections.singletonList(category.getName()));
                            productResponseList.add(prodResponse);
                        }
                    }
                    break;
                }
            }
        } else {
            if (categories.isEmpty()) {
                productList = productRepository.findProductWithoutCategory();

                for (Product product : productList) {
                    productResponseList.add(new ProductResponse(product));
                }
            }
            // categories is not Empty
            else {
                switch (order) {
                    case "product": {
                        for (Integer categoryId : categories) {
                            productList.addAll(productRepository.findProductListByCategory(categoryId));
                        }
                        Set<Product> productSet = new TreeSet<Product>(Comparator.comparing(Product::getName));
                        productSet.addAll(productList);

                        for (Product product : productSet) {
                            productResponse = new ProductResponse(product);
                            for (Category category : product.getCategories()) {
                                productResponse.getCategoriesNames().add(category.getName());
                            }

                            productResponseList.add(productResponse);
                        }
                        break;
                    }
                    case "category": {
                        List<Category> categoryList = new ArrayList<>();
                        for (Integer categoryId : categories) {
                            categoryList.add(categoryService.getCategoryById(categoryId));
                        }

                        for (Category category : categoryList) {
                            productList.addAll(productRepository.findProductListByCategory(category.getId()));
                            for (Product product : productList) {
                                ProductResponse prodResponse = new ProductResponse(product);
                                productResponse.setCategoriesNames(Collections.singletonList(category.getName()));
                                productResponseList.add(prodResponse);
                            }
                        }
                        break;
                    }
                }
            }
        }

        return productResponseList;
    }


    @Override
    public List<ProductResponse> addAllProducts(List<ProductRequest> productRequestList) {
        List<ProductResponse> productResponseList = new ArrayList<>();
        for (ProductRequest productRequest : productRequestList) {
            productResponseList.add(addProduct(productRequest));
        }
        return productResponseList;
    }


}
