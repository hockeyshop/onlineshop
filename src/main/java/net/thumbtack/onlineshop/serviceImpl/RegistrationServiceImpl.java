package net.thumbtack.onlineshop.serviceImpl;

import net.thumbtack.onlineshop.exeptions.UserErrorCode;
import net.thumbtack.onlineshop.exeptions.UserException;
import net.thumbtack.onlineshop.model.User;
import net.thumbtack.onlineshop.model.modelRequest.UserRequest;
import net.thumbtack.onlineshop.model.modelResponse.AdminResponse;
import net.thumbtack.onlineshop.model.modelResponse.ClientResponse;
import net.thumbtack.onlineshop.model.modelResponse.UserResponse;
import net.thumbtack.onlineshop.repository.RoleRepository;
import net.thumbtack.onlineshop.repository.UserRepository;
import net.thumbtack.onlineshop.security.Role;
import net.thumbtack.onlineshop.service.RegistrationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Service
public class RegistrationServiceImpl implements RegistrationService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private RoleRepository roleRepository;

    // Добавление нового администратора
    @Override
    public UserResponse saveAdmin(UserRequest request) throws UserException {

        if (userRepository.findByLogin(request.getLogin()) != null) {
            throw new UserException(UserErrorCode.LOGIN_EXISTS, "login", "Login" + request.getLogin() + "are registered");
        }

        User user = new User(request);
//        roleRepository.save(new Role(2, "ROLE_ADMIN"));
//        Role role = roleRepository.save(new Role(2, "ROLE_ADMIN"));
        user.getRoles().add(roleRepository.save(new Role(2, "ROLE_ADMIN")));
        userRepository.save(user);
        return new AdminResponse(userRepository.save(user));
    }

    // Добавление нового пользователя
    @Override
    public UserResponse saveUser(UserRequest request) throws UserException {

        if (userRepository.findByLogin(request.getLogin()) != null) {
            throw new UserException(UserErrorCode.LOGIN_EXISTS, "login", "Login" + request.getLogin() + "are registered");
        }

//        User user = new User(request);
//        Role role = roleRepository.save(new Role(1, "ROLE_USER"));
//        user.getRoles().add(role);

        User user = new User(request);
        user.getRoles().add(roleRepository.save(new Role(1, "ROLE_USER")));
        userRepository.save(user);

        return new ClientResponse(userRepository.save(user));
    }

    // Login
    @Override
    public UserResponse userLogin(String login, String password) {
        UserResponse userResponse;
        if (userRepository.findByLogin(login) == null) {
            throw new UserException(UserErrorCode.LOGIN_WRONG, "login", "Пользователя с таким логином не существует");
        }
        User user = userRepository.findByLogin(login);
        if (!user.getPassword().equals(password)) {
            throw new UserException(UserErrorCode.PASSWORD_NOT_CORRECT, "password", "Введен неверный пароль");
        }
        if (user.getRoles().contains(new Role(1, "ROLE_USER"))){
            userResponse = new ClientResponse(user);
        }
        else {
            userResponse = new AdminResponse(user);
        }
        return userResponse;
    }

    // Редактирование профиля администратора
    @Override
    public UserResponse updateAdmin(UserRequest userRequest, Integer userId) {
        User userFromDB = userRepository.getOne(userId);
        if (userFromDB.getPassword().equals(userRequest.getPassword())) {
            userFromDB.setFirstName(userRequest.getFirstName());
            userFromDB.setLastName(userRequest.getLastName());
            userFromDB.setPatronymic(userRequest.getPatronymic());
            userFromDB.setPassword(userRequest.getNewPassword());
        }
        userFromDB = userRepository.save(userFromDB);
        return new AdminResponse(userFromDB);
    }

    // Редактирование профиля клиента
    @Override
    public UserResponse updateUser(UserRequest userRequest, Integer userId) {
        User userFromDB = userRepository.getOne(userId);
        if (userFromDB.getPassword().equals(userRequest.getPassword())) {
            userFromDB.setFirstName(userRequest.getFirstName());
            userFromDB.setLastName(userRequest.getLastName());
            userFromDB.setEmail(userRequest.getEmail());
            userFromDB.setAddress(userRequest.getAddress());
            userFromDB.setPhone(userRequest.getPhone());
            userFromDB.setPassword(userRequest.getNewPassword());
            if (userRequest.getPatronymic() != null) {
                userFromDB.setPatronymic(userRequest.getPatronymic());
            }
        }
        userFromDB = userRepository.save(userFromDB);
        return new ClientResponse(userFromDB);
    }

    // сохранить список пользователей
    public List<User> saveAllUsers(List<User> users) {
        roleRepository.save(new Role(1, "ROLE_USER"));
        for (User user : users) {
            user.setRoles(Collections.singleton(roleRepository.getOne(1)));
        }
        return userRepository.saveAll(users);
    }

    // получение информации о клиентах
//    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @Override
    public List<UserResponse> findAllUsers() {
        List<UserResponse> userResponseList = new ArrayList<>();
        List<User> userList = userRepository.findAll();
        userList.forEach(user -> {
            if (user.getPosition() != null){
                userResponseList.add(new AdminResponse(user));
            }
            else{
                userResponseList.add(new ClientResponse(user));
            }});
        return userResponseList;
    }

    // получить информацию о текущем пользователе
    @Override
    public UserResponse findUserById(Integer id) throws UserException {
        UserResponse userResponse;
        if (userRepository.findById(id).isPresent()){
            User user = userRepository.getOne(id);
            if (user.getPosition() != null){
                userResponse = new AdminResponse(user);
            }
            else{
                userResponse = new ClientResponse(user);
            }
        }
        else {
            throw new UserException(UserErrorCode.NOT_USER_FIND, "ID" + id, "ID = " + id + "NOT FOUND");
        }
        return userResponse;
    }

    // получить пользователя по ID
    @Override
    public User getUserById(Integer id) throws UserException {
        return userRepository.findById(id).orElseThrow(() -> new UserException(UserErrorCode.NOT_USER_FIND, "ID" + id, "ID = " + id + "NOT FOUND"));
    }

    // удаление пользователя
    @Override
    public void userDeleteById(Integer id) {
        userRepository.deleteById(id);
    }

    // сохранить список администраторов
    public List<User> saveAllAdmins(List<User> users) {
        roleRepository.save(new Role(2, "ROLE_ADMIN"));
        for (User user : users) {
            user.setRoles(Collections.singleton(roleRepository.getOne(2)));
        }
        return userRepository.saveAll(users);
    }

}
