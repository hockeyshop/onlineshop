package net.thumbtack.onlineshop.controller;


import com.fasterxml.jackson.annotation.JsonView;
import net.thumbtack.onlineshop.model.modelRequest.ProductRequest;
import net.thumbtack.onlineshop.model.modelResponse.ProductResponse;
import net.thumbtack.onlineshop.model.modelViews.ProductView;
import net.thumbtack.onlineshop.model.modelViews.UserView;
import net.thumbtack.onlineshop.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.Cookie;
import java.util.List;

@RestController
@RequestMapping(value = "/api", produces = "application/json; charset=utf-8")
public class ProductController {
    @Autowired
    private ProductService productService;

    // добавление нового товара
    @PostMapping("/products")
    @JsonView(ProductView.ProductIdView.class)
    public ProductResponse addProduct(@CookieValue(value = "JAVASESSIONID") Cookie cookie,
                                      @RequestBody ProductRequest productRequest) {
        return productService.addProduct(productRequest);
    }

    // редактирование товара
    @PutMapping("/products/{id}")
    @JsonView(ProductView.ProductIdView.class)
    public ProductResponse updateProduct(@CookieValue(value = "JAVASESSIONID") Cookie cookie,
                                         @PathVariable("id") Integer id,
                                         @RequestBody ProductRequest productRequest) {
        return productService.updateProduct(id, productRequest);
    }

    // удаление товара
    @DeleteMapping("/products/{id}")
    public String deleteProduct(@CookieValue(value = "JAVASESSIONID") Cookie cookie,
                                @PathVariable("id") Integer id) {
        return productService.deleteProduct(id);
    }

    // получение информации о товаре
//    @PreAuthorize("hasAuthority('ROLE_ADMIN') or hasAuthority('ROLE_USER')")
    @GetMapping("/products/{id}")
    @JsonView(ProductView.ProductNameView.class)
    public ProductResponse infoProduct(@CookieValue(value = "JAVASESSIONID") Cookie cookie,
                                       @PathVariable("id") Integer id) {
        return productService.infoProduct(id);
    }

    // получение списка товаров
//    @PreAuthorize("hasAuthority('ROLE_ADMIN') or hasAuthority('ROLE_USER')")
    @GetMapping("/products")
    @JsonView(ProductView.ProductNameView.class)
    public List<ProductResponse> infoProductList(@CookieValue(value = "JAVASESSIONID") Cookie cookie,
                                                 @RequestParam(value = "category", required=false) List<Integer> categories,
                                                 @RequestParam(value = "order", defaultValue = "product") String order) {
        return productService.infoProductList(categories, order);
    }

    // добавление списка товаров
    @PostMapping("/products/all")
    public List<ProductResponse> addAllProducts(@RequestBody List<ProductRequest> productRequest){
        return productService.addAllProducts(productRequest);
    }


}
