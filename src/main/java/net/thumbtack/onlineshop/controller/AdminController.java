package net.thumbtack.onlineshop.controller;

import net.thumbtack.onlineshop.model.modelCheck.UserCheck;
import net.thumbtack.onlineshop.model.modelRequest.CategoryRequest;
import net.thumbtack.onlineshop.model.modelResponse.CategoryResponse;
import net.thumbtack.onlineshop.service.AdminService;
import net.thumbtack.onlineshop.service.CategoryService;
import net.thumbtack.onlineshop.service.RegistrationService;
import net.thumbtack.onlineshop.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.Cookie;
import java.util.List;

@RestController
@RequestMapping(value = "/api", produces = "application/json; charset=utf-8")
public class AdminController {
    @Autowired
    private CategoryService categoryService;
    @Autowired
    private RegistrationService registrationService;
    @Autowired
    private AdminService adminService;
    @Autowired
    private UserCheck userCheck;
    @Autowired
    private UserService userService;

    public AdminController() {
    }


    // добавление новой категории (подкатегории)
    @PostMapping("/categories")
    public CategoryResponse addCategory(@CookieValue(value = "JAVASESSIONID") Cookie cookie,
                                        @RequestBody CategoryRequest categoryRequest) {
        if (categoryRequest.getParentId() == null || categoryRequest.getParentId() == 0) {
            return categoryService.addCategory(categoryRequest.getCategoryName());
        } else {
            return categoryService.addSubCategory(categoryRequest.getCategoryName(), categoryRequest.getParentId());
        }
    }

    // получение категории (подкатегории) по её номеру
    @GetMapping("/categories/{num}")
    public CategoryResponse getCategoryByNumber(@CookieValue(value = "JAVASESSIONID") Cookie cookie,
                                                @PathVariable("num") Integer num) {
        return categoryService.getListCategories().get(num);
    }

    // редактирование категории (подкатегории) по её номеру
    @PutMapping("/categories/{num}")
    public CategoryResponse editCategoryByNumber(@CookieValue(value = "JAVASESSIONID") Cookie cookie,
                                                 @PathVariable("num") Integer num,
                                                 @RequestBody CategoryRequest categoryRequest) {
        return categoryService.editCategoryByNumber(num, categoryRequest);
    }

    // удаление категории (подкатегории) по её ID
    @DeleteMapping("/categories/{id}")
    public String delCategoryById(@CookieValue(value = "JAVASESSIONID") Cookie cookie,
                                  @PathVariable("id") Integer id) {
        return categoryService.delCategoryById(id);
    }

    // удаление всех категорий и подкатегорий
    @GetMapping("/categories/delete")
    public void delCategoryById(@CookieValue(value = "JAVASESSIONID") Cookie cookie) {
        categoryService.delCategoryAll();
    }

    // получение списка категорий с подкатегориями
    @GetMapping("/categories")
    public List<CategoryResponse> getListCategories(@CookieValue(value = "JAVASESSIONID") Cookie cookie) {
        return categoryService.getListCategories();
    }


}
