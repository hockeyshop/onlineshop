package net.thumbtack.onlineshop.controller;

import net.thumbtack.onlineshop.model.modelCheck.UserCheck;
import net.thumbtack.onlineshop.model.modelRequest.PurchaseRequest;
import net.thumbtack.onlineshop.model.modelResponse.PurchaseFromBasketsResponse;
import net.thumbtack.onlineshop.model.modelResponse.PurchaseResponse;
import net.thumbtack.onlineshop.model.modelResponse.UserResponse;
import net.thumbtack.onlineshop.service.RegistrationService;
import net.thumbtack.onlineshop.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.Cookie;
import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping(value = "/api", produces = "application/json; charset=utf-8")
public class UserController {
    @Autowired
    private RegistrationService registrationService;
    @Autowired
    private UserService userService;
    @Autowired
    private UserCheck userCheck;

    public UserController() {
    }

    // внесение денег на счет
    @PutMapping("/deposits")
    public UserResponse setMoney(@CookieValue(value = "JAVASESSIONID") Cookie cookieName,
                                 @RequestBody Integer deposit) {
        Integer userId = Integer.valueOf(cookieName.getValue());
        return userService.depMoney(userId, deposit);
    }

    // получение суммы денег, имеющейся на счете
    @GetMapping("/deposits")
    public UserResponse getMoney(@CookieValue(value = "JAVASESSIONID") Cookie cookieName) {
        Integer userId = Integer.valueOf(cookieName.getValue());
        return userService.getMoney(userId);
    }

    // покупка товара
    @PostMapping("/purchases")
    public PurchaseResponse purchase(@CookieValue(value = "JAVASESSIONID") Cookie cookieName,
                                     @Valid @RequestBody PurchaseRequest purchaseRequest) {
        Integer userId = Integer.valueOf(cookieName.getValue());
        if (purchaseRequest.getProductCount() == null) {
            purchaseRequest.setProductCount(1);
        }
        return userService.purchase(purchaseRequest, userId);
    }

    // добавление товара в корзину
    @PostMapping("/baskets")
    public List<PurchaseResponse> addProductInBaskets(@CookieValue(value = "JAVASESSIONID") Cookie cookieName,
                                                      @Valid @RequestBody PurchaseRequest purchaseRequest) {
        Integer userId = Integer.valueOf(cookieName.getValue());
        if (purchaseRequest.getProductCount() == null) {
            purchaseRequest.setProductCount(1);
        }
        return userService.addProductInBaskets(purchaseRequest, userId);
    }

    // удаление товара из корзины
    @DeleteMapping("/baskets/{productId}")
    public String deleteProductInBaskets(@CookieValue(value = "JAVASESSIONID") Cookie cookieName,
                                         @PathVariable("productId") Integer productId) {
        Integer userId = Integer.valueOf(cookieName.getValue());
        return userService.deleteProductInBaskets(userId, productId);
    }

    // изменение количества товара в корзине
    @PutMapping("/baskets")
    public List<PurchaseResponse> updateProductInBaskets(@CookieValue(value = "JAVASESSIONID") Cookie cookieName,
                                                      @Valid @RequestBody PurchaseRequest purchaseRequest) {
        Integer userId = Integer.valueOf(cookieName.getValue());
        if (purchaseRequest.getProductCount() == null) {
            purchaseRequest.setProductCount(1);
        }
        return userService.updateProductInBaskets(purchaseRequest, userId);
    }

    // получение состава корзины
    @GetMapping("/baskets")
    public List<PurchaseResponse> getUserBaskets(@CookieValue(value = "JAVASESSIONID") Cookie cookieName) {
        Integer userId = Integer.valueOf(cookieName.getValue());
        return userService.getUserBaskets(userId);
    }

    // покупка товаров из корзины
    @PostMapping("/purchases/baskets")
    public PurchaseFromBasketsResponse purchaseFromBaskets(@CookieValue(value = "JAVASESSIONID") Cookie cookieName,
                                                           @Valid @RequestBody List<PurchaseRequest> purchaseRequestList) {
        Integer userId = Integer.valueOf(cookieName.getValue());
        return userService.purchaseFromBaskets(purchaseRequestList, userId);
    }
}
