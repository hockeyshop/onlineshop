package net.thumbtack.onlineshop.controller;

import com.fasterxml.jackson.annotation.JsonView;
import net.thumbtack.onlineshop.exeptions.UserException;
import net.thumbtack.onlineshop.model.User;
import net.thumbtack.onlineshop.model.modelCheck.UserCheck;
import net.thumbtack.onlineshop.model.modelRequest.UserRequest;
import net.thumbtack.onlineshop.model.modelRequest.LoginUserRequest;
import net.thumbtack.onlineshop.model.modelResponse.AdminResponse;
import net.thumbtack.onlineshop.model.modelResponse.UserResponse;
import net.thumbtack.onlineshop.model.modelViews.UserView;
import net.thumbtack.onlineshop.security.Role;
import net.thumbtack.onlineshop.security.TokenHandler;
import net.thumbtack.onlineshop.security.UserAuthentication;
import net.thumbtack.onlineshop.security.UserAuthenticationManager;
import net.thumbtack.onlineshop.service.AdminService;
import net.thumbtack.onlineshop.service.RegistrationService;
import net.thumbtack.onlineshop.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.util.List;

import static net.thumbtack.onlineshop.validate.UserRequestValidation.userRequestValidation;

@RestController
@RequestMapping(produces = "application/json; charset=utf-8")
public class RegistrationController {
    @Autowired
    private RegistrationService registrationService;
    @Autowired
    private AdminService adminService;
    @Autowired
    private UserService userService;
    @Autowired
    private UserCheck userCheck;
    @Autowired
    private TokenHandler tokenHandler;

    public RegistrationController() {
    }

    // добавление администратора
    @PostMapping("/api/admins")
//    @JsonView(UserView.AdminView.class)
    public UserResponse adminAdd(@Valid @RequestBody UserRequest userRequest, BindingResult bindingResult, HttpServletResponse response) throws Exception {
        UserResponse userResponse = registrationService.saveAdmin(userRequestValidation(userRequest, bindingResult));
        userLogin(new LoginUserRequest(userRequest.getLogin(), userRequest.getPassword()), response);
        return userResponse;
    }

    // добавление клиента
    @PostMapping("/api/clients")
//    @JsonView(UserView.ClientFullView.class)
    public UserResponse userAdd(@Valid @RequestBody UserRequest userRequest, BindingResult bindingResult, HttpServletResponse response) throws Exception {
        UserResponse userResponse = registrationService.saveUser(userRequestValidation(userRequest, bindingResult));
        userLogin(new LoginUserRequest(userRequest.getLogin(), userRequest.getPassword()), response);
        return userResponse;
    }

    //  Login
    @PostMapping("/api/sessions")
//    @JsonView(UserView.ShortView.class)
    public UserResponse userLogin(//@CookieValue(value = "JAVASESSIONID") Cookie cookie,
                          @RequestBody LoginUserRequest loginUserRequest,
                          HttpServletResponse response) {

//        request.getSession(true);

        UserResponse userResponse = registrationService.userLogin(loginUserRequest.getLogin(), loginUserRequest.getPassword());
        User user = registrationService.getUserById(userResponse.getId());

        Authentication authentication = new UsernamePasswordAuthenticationToken(user, null, user.getAuthorities());
        SecurityContextHolder.getContext().setAuthentication(authentication);

        String token = tokenHandler.generateAccessToken(user.getId());
        Cookie cookie = new Cookie("JAVASESSIONID", token);
//        Cookie cookie = new Cookie("JAVASESSIONID", Integer.toString(user.getId()));
        response.addCookie(cookie);

        System.out.println("USER LOGIN");
        System.out.println("User login: " + user.getLogin());
        user.getRoles().forEach(role -> {
            System.out.println(role.getAuthority());
        });

        return userResponse;
    }

    //  Logout
    @GetMapping("/api/sessions")
//    @JsonView(UserView.ShortView.class)
    public String userLogout(HttpSession session, HttpServletResponse response) {

        Cookie userNameCookieRemove = new Cookie("JAVASESSIONID", "");
        userNameCookieRemove.setMaxAge(0);
        response.addCookie(userNameCookieRemove);

        session.invalidate();
        SecurityContextHolder.clearContext();

        return "";
    }

    // Получение информации о текущем пользователе
    @GetMapping("/api/accounts")
//    @JsonView(UserView.FullView.class)
    public UserResponse findUser(@CookieValue(value = "JAVASESSIONID") Cookie cookie) throws UserException {
        Integer userId = Integer.valueOf(tokenHandler.extractUserId(cookie.getValue()));
        UserResponse userResponse = registrationService.findUserById(userId);
        return userResponse;
    }

    // получение информации о клиентах
//    @PreAuthorize("hasRole('ROLE_ADMIN')")
//    @Secured("ROLE_ADMIN")
    @GetMapping("/api/clients")
    public List<UserResponse> findAllUser() {
        return registrationService.findAllUsers();
    }

    // редактирование профиля администратора
    @PutMapping("/api/admins")
//    @JsonView(UserView.AdminView.class)
    public UserResponse editAdmin(@CookieValue(value = "JAVASESSIONID") Cookie cookie,
                                  @Valid @RequestBody UserRequest userRequest) {
        Integer userId = Integer.valueOf(tokenHandler.extractUserId(cookie.getValue()));
        return registrationService.updateAdmin(userRequest, userId);
    }

    // редактирование данных пользователя
    @PutMapping("/api/clients")
//    @JsonView(UserView.ClientFullView.class)
    public UserResponse editUser(@CookieValue(value = "JAVASESSIONID") Cookie cookie,
                                 @Valid @RequestBody UserRequest userRequest) {
        Integer userId = Integer.valueOf(tokenHandler.extractUserId(cookie.getValue()));
        return registrationService.updateUser(userRequest, userId);
    }

    // удаление пользователя
    @DeleteMapping("/api/delete/{id}")
    public void deleteUser(@PathVariable("id") Integer id) {
        registrationService.userDeleteById(id);
    }

    // стартовая страница
    @GetMapping("/")
    public String startPage() {
        return "Start OnlineShop";
    }

//****************************************************//

    // добавление списка администраторов
    @PostMapping("/api/admins/all")
    @JsonView(UserView.AdminView.class)
    public List<User> saveAllAdmins(@RequestBody List<User> users) {
        return registrationService.saveAllAdmins(users);
    }

    // добавление списка клиентов
    @PostMapping("/api/clients/all")
    @JsonView(UserView.ClientFullView.class)
    public List<User> saveAllUsers(@RequestBody List<User> users) {
        for (User user : users) {
            user.setDeposit(0);
        }
        return registrationService.saveAllUsers(users);
    }

    // получить информацию о пользователе по его ID
    @GetMapping("/api/find/{id}")
    @JsonView(UserView.FullView.class)
    public User findUser(@PathVariable("id") Integer id) throws UserException {
        return registrationService.getUserById(id);
    }


}
