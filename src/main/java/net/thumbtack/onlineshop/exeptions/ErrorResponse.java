package net.thumbtack.onlineshop.exeptions;

import org.springframework.http.HttpStatus;

public class ErrorResponse{
    private UserErrorCode errorCode;    // "код ошибки"
    private String field;     // "поле запроса, являющееся причиной ошибки"
    private String message;   // "причина ошибки"
    HttpStatus httpStatus;

    public ErrorResponse()
    {
        super();
    }
    public ErrorResponse(UserErrorCode errorCode, String message)
    {
        super();
        this.errorCode = errorCode;
        this.message = message;
    }

    public ErrorResponse(UserErrorCode errorCode, String field, String message) {
        super();
        this.errorCode = errorCode;
        this.field = field;
        this.message = message;
    }

    public ErrorResponse(String field, String message) {
        super();
        this.field = field;
        this.message = message;
    }


    public UserErrorCode getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(UserErrorCode errorCode) {
        this.errorCode = errorCode;
    }

    public String getField() {
        return field;
    }

    public void setField(String field) {
        this.field = field;
    }

    public String getMessage()
    {
        return message;
    }
    public void setMessage(String message)
    {
        this.message = message;
    }
    @Override
    public String toString()
    {
        return "ErrorResponse [errorCode: " + errorCode + ", field: " + field + ", message: " + message + "]";
    }
}
