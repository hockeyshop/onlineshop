package net.thumbtack.onlineshop.exeptions;


import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.io.IOException;

@ControllerAdvice
public class RestExceptionHandler extends ResponseEntityExceptionHandler {

    @ExceptionHandler(UserException.class)
    public ResponseEntity<ErrorResponse> handleThereIsNoSuchUserException(UserException userExeption) {
        return new ResponseEntity<>(new ErrorResponse (
                userExeption.getErrorCode(), userExeption.getField(), userExeption.getMessage()),
                HttpStatus.BAD_REQUEST);
    }
}
