package net.thumbtack.onlineshop.exeptions;

import org.springframework.stereotype.Component;


public enum UserErrorCode {
    INCORRECT_DATE,
    FIRST_NAME_WRONG,
    LAST_NAME_WRONG,
    LOGIN_WRONG,
    LOGIN_EXISTS,
    PASSWORD_NOT_CORRECT,
    NOT_USER_FIND,
    NOT_USER_TOKEN,
    INCORRECT_END_OPERATION,
    USER_NOT_LOGIN,
    USER_ALREADY_LOGIN,
    REQUEST_WRONG,
    PRICE_NOT_CORRECT,
    NAME_NOT_CORRECT
}
