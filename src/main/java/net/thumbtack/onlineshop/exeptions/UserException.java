package net.thumbtack.onlineshop.exeptions;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RestControllerAdvice;

//@RestControllerAdvice
public class UserException extends RuntimeException {
    private UserErrorCode errorCode;    // "код ошибки"
    private String field;               // "поле запроса, являющееся причиной ошибки"
    private String message;             // "причина ошибки"

    public UserException() {
        super();
    }

    public UserException(UserErrorCode errorCode) {
        super();
        this.errorCode = errorCode;
    }

    public UserException(UserErrorCode errorCode, String field, String message) {
        super();
        this.errorCode = errorCode;
        this.field = field;
        this.message = message;
    }

//    public UserException(String code, String field, String message) {
//        super();
//        this.errorCode = code;
//        this.field = field;
//        this.message = message;
//    }

    @Override
    public String toString() {
        return "UserException{" +
                "errorCode=" + errorCode +
                ", field='" + field + '\'' +
                ", message='" + message + '\'' +
                '}';
    }

    public UserErrorCode getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(UserErrorCode errorCode) {
        this.errorCode = errorCode;
    }

    public String getField() {
        return field;
    }

    public void setField(String field) {
        this.field = field;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
