DROP DATABASE IF EXISTS onlineshop;
CREATE DATABASE onlineshop;
USE onlineshop;

-- CREATE TABLE user (
--  id INT(11) NOT NULL AUTO_INCREMENT,
--  firstName VARCHAR(50) NOT NULL,
--  lastName VARCHAR(50) NOT NULL,
--  patronymic VARCHAR(50),
--  email VARCHAR(50),
--  address VARCHAR(50),
--  phone VARCHAR(13),
--  deposit INT(10),
--  login VARCHAR(50) NOT NULL,
--  password VARCHAR(50) NOT NULL,
--  PRIMARY KEY (id),
--  unique key login (login),
--  unique key password (password),
--  INDEX firstName (firstName),
--  INDEX lastName (lastName),
--  INDEX patronymic (patronymic),
--  INDEX email (email),
--  INDEX address (address),
--  INDEX phone (phone)
--  ) ENGINE=INNODB DEFAULT CHARSET=utf8;
--
-- CREATE TABLE admin (
--  id INT(11) NOT NULL AUTO_INCREMENT,
--  firstName VARCHAR(50) NOT NULL,
--  lastName VARCHAR(50) NOT NULL,
--  patronymic VARCHAR(50),
--  position VARCHAR(50) NOT NULL,
--  login VARCHAR(50) NOT NULL,
--  password VARCHAR(50) NOT NULL,
--  PRIMARY KEY (id),
--  unique key login (login),
--  unique key password (password),
--  INDEX firstName (firstName),
--  INDEX lastName (lastName),
--  INDEX patronymic (patronymic),
--  INDEX position (position)
--  ) ENGINE=INNODB DEFAULT CHARSET=utf8;
--
-- CREATE TABLE product (
--  id INT(11) NOT NULL AUTO_INCREMENT,
--  name VARCHAR(50) NOT NULL,
--  price INT(10) NOT NULL,
--  count INT(10) NOT NULL,
--  PRIMARY KEY (id),
--  UNIQUE KEY name (name)
--  ) ENGINE=INNODB DEFAULT CHARSET=utf8;
--
-- CREATE TABLE categories (
--  id INT(11) NOT NULL AUTO_INCREMENT,
--  name VARCHAR(50) NOT NULL,
--  PRIMARY KEY (id),
--  UNIQUE KEY name (name)
--  ) ENGINE=INNODB DEFAULT CHARSET=utf8;
--
-- CREATE TABLE subcategories (
--  id INT(11) NOT NULL AUTO_INCREMENT,
--  name VARCHAR(50) NOT NULL,
--  parent_id INT(11) NOT NULL,
--  PRIMARY KEY (id),
--  UNIQUE KEY name (name),
--  FOREIGN KEY (parent_id) REFERENCES categories (id) ON DELETE CASCADE
--  ) ENGINE=INNODB DEFAULT CHARSET=utf8;
--
-- CREATE TABLE product_categories (
--  id INT(11) NOT NULL AUTO_INCREMENT,
--  product_id INT(11) NOT NULL,
--  category_id INT(11) NOT NULL,
--  PRIMARY KEY (id),
--  FOREIGN KEY (product_id) REFERENCES product (id) ON DELETE CASCADE,
--  FOREIGN KEY (category_id) REFERENCES subcategories (id) ON DELETE CASCADE
--  ) ENGINE=INNODB DEFAULT CHARSET=utf8;
--
-- CREATE TABLE user_basket (
--  id INT(11) NOT NULL AUTO_INCREMENT,
--  user_id INT(11) NOT NULL,
--  product_id INT(11) NOT NULL,
--  PRIMARY KEY (id),
--  FOREIGN KEY (user_id) REFERENCES user (id) ON DELETE CASCADE,
--  FOREIGN KEY (product_id) REFERENCES product (id) ON DELETE CASCADE
--  ) ENGINE=INNODB DEFAULT CHARSET=utf8;
--
-- CREATE TABLE logined_users (
--  id INT(11) NOT NULL AUTO_INCREMENT,
--  user_id INT(11) NOT NULL,
--  PRIMARY KEY (id),
--  FOREIGN KEY (user_id) REFERENCES user (id) ON DELETE CASCADE
--  ) ENGINE=INNODB DEFAULT CHARSET=utf8;